import React, { Component } from "react";
import radium from "radium";

class ProgressBar extends Component {
  static styles = {};
  render() {
    const styles = ProgressBar.styles;
    const progressPercent = this.props.progressPercent;
    return (
      <div
        style={[
          styles.progressbarContainer,
          this.props.progressBarContainerStyle
        ]}
      >
        <div style={[styles.progressBar, this.props.progressBarStyle]}>
          <div
            style={[
              styles.progressBarFill,
              this.props.progressBarFillStyle,
              { right: 100 - progressPercent + "%" },
              this.props.progressBarFillColor
            ]}
          />
        </div>
      </div>
    );
  }
}

ProgressBar.styles = {
  progressbarContainer: {
    width: "207px",
    float: "left"
  },
  progressBar: {
    backgroundColor: "rgba(219, 226, 232, 0.72)",
    width: "100%",
    border: "none",
    height: "10px",
    borderRadius: "7px",
    overflow: "hidden",
    position: "relative",
    WebkitMaskImage:
      "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAA5JREFUeNpiYGBgAAgwAAAEAAGbA+oJAAAAAElFTkSuQmCC)"
  },
  progressBarFill: {
    borderRadius: "7px",
    height: "10px",
    backgroundColor: "#0abff1",
    position: "relative",
    width: "100%"
  }
};

export default radium(ProgressBar);
