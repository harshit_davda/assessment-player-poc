import React from "react";
import { observer } from "mobx-react";
import themeConfig from "@lyearn-team/theme-config";
import { ThemeProvider } from "emotion-theming";
import props from "./helper/props";
import cloneDeep from "lodash/cloneDeep";
import keyBy from "lodash/keyBy";
import {
  mtfFunction,
  ftbFunction,
  evaluateSelectResponse,
  formatAnswerForCommand,
  getScaledScoreForRawScore,
  getSuccessStatusForLeafNode
} from "./helper";
import {
  getObjectiveResponseType,
  checkQuestionCompleted
} from "./NewObjectiveAssessment/helpers";
import NewObjectiveAssessment from "./NewObjectiveAssessment";

import * as s from "./style";

@observer
class App extends React.Component {
  constructor(data) {
    super(data);
    this.state = {
      cardData: props.cardData,
      isMobile: window.matchMedia("(max-width: 767px)").matches
    };
  }

  checkIsMobile = e => {
    this.setState({ isMobile: e.matches });
  };

  componentDidMount() {
    this.match = window.matchMedia("(max-width: 767px)");
    this.match.addListener(this.checkIsMobile);
  }

  componentWillUnmount() {
    this.match.removeListener(this.checkIsMobile);
  }

  addQuestionSubmission = props => {
    const { answers } = props;
    const cardData = this.updateSubmissions(
      answers.map(formatAnswerForCommand)
    );
    this.evaluateContent();
    this.setState({ cardData: { ...cardData } });
  };

  updateSubmissions = answers => {
    const answersByQuestionId = keyBy(answers, "questionId");
    const cardData = this.state.cardData;
    cardData.card.children.forEach(questionContent => {
      if (answersByQuestionId[questionContent.cardQuestion._id]) {
        const answer = answersByQuestionId[questionContent.cardQuestion._id];
        const timeStamp = new Date().toISOString();
        const state = questionContent.state;
        state.submissions[0].response = answer.response;
        state.submissions[0].responseType = answer.responseType;
        state.submissions[0].answerEvaluation = answer.answerEvaluation;
        state.submissions[0].submittedOn = timeStamp;

        state.progressMeasure = state.submissions[0].progressMeasure =
          answer.progressMeasure;

        state.rawScore = state.submissions[0].rawScore = answer.rawScore;
        state.submittedOn = state.lastSubmittedOn = timeStamp;
        state.lastAccessedOn = state.updatedAt = timeStamp;
      }
    });

    return cardData;
  };

  evaluateContent = () => {
    const { state, card } = this.state.cardData;

    let allQuestionsGraded = true;
    let totalRawScore = 0;
    let totalMinScore = 0;
    let totalMaxScore = 0;

    card.children.forEach(
      ({ cardQuestion: questionContent, state: questionState }) => {
        totalRawScore += questionState.rawScore;
        totalMinScore += questionContent.minScore;
        totalMaxScore += questionContent.maxScore;
      }
    );

    state.rawScore = totalRawScore;
    state.scaledScore = getScaledScoreForRawScore(
      totalRawScore,
      totalMinScore,
      totalMaxScore
    );

    state.successStatus = getSuccessStatusForLeafNode(
      card.successCriteria,
      state.scaledScore,
      card.minScaledScore,
      state.completionStatus,
      allQuestionsGraded
    );
  };

  perQuestionSubmission = props => {
    const { answerObject } = props;
    const answer = formatAnswerForCommand(answerObject);
    const cardData = this.updateSubmissions([answer]);
    this.setState({ cardData: { ...cardData } });
  };

  dispatch = async (type, payload) => {
    switch (type) {
      case "FINISH_TEST": {
        const getRawScore = ({
          answerEvaluation,
          maxScore,
          minScore,
          penaltyMarks
        }) => {
          if (answerEvaluation === "TRUE") {
            return maxScore;
          }
          if (answerEvaluation === "NA") {
            return minScore;
          }
          return minScore - penaltyMarks;
        };
        const { children } = payload.card;
        const answersList = [];
        const penaltyMarks =
          payload.card.isGradable && payload.card.questionSettings
            ? payload.card.questionSettings.penaltyMarks || 0
            : 0;
        children.forEach(item => {
          const questionType = getObjectiveResponseType(item.cardQuestion);
          switch (questionType) {
            case "MATCH_THE_FOLLOWING": {
              const { cardQuestion, state } = item;
              const { maxScore = 1, minScore = 0, content } = cardQuestion;
              const { pairs } = content.question;
              const response = state.submissions
                ? state.submissions[0].response || "[]"
                : "[]";
              const parsedResponse = JSON.parse(response);
              const questionId = cardQuestion._id;
              const questionComplete = checkQuestionCompleted({
                currentQuestion: cardQuestion,
                currentQuestionState: state
              });
              const answerEvaluation = questionComplete
                ? mtfFunction.evalMTFAns(parsedResponse, pairs)
                : "NA";
              const answer = {
                response,
                questionId,
                responseType: questionType,
                timeStamp: new Date().toISOString(),
                progressMeasure: 1,
                answerEvaluation,
                rawScore: getRawScore({
                  answerEvaluation,
                  maxScore,
                  minScore,
                  penaltyMarks
                })
              };
              answersList.push(answer);
              break;
            }
            case "FILL_IN_THE_BLANKS": {
              const { cardQuestion, state } = item;
              const { maxScore = 1, minScore = 0 } = cardQuestion;
              const response = state.submissions
                ? state.submissions[0].response || "{}"
                : "{}";
              let parsedResponse;
              try {
                parsedResponse = JSON.parse(response);
              } catch (err) {
                parsedResponse = {};
              }
              let answerEvaluation = "FALSE";
              const questionId = cardQuestion._id;
              const questionComplete = checkQuestionCompleted({
                currentQuestion: cardQuestion,
                currentQuestionState: state
              });
              answerEvaluation = questionComplete
                ? ftbFunction.evalFTBAnswer(parsedResponse)
                : "NA";
              const answer = {
                response,
                questionId,
                responseType: questionType,
                timeStamp: new Date().toISOString(),
                progressMeasure: 1,
                answerEvaluation,
                rawScore: getRawScore({
                  answerEvaluation,
                  maxScore,
                  minScore,
                  penaltyMarks
                })
              };
              answersList.push(answer);
              break;
            }
            case "MULTIPLE_SELECT":
            case "SINGLE_SELECT": {
              const { cardQuestion, state } = item;
              const questionResponseType = getObjectiveResponseType(
                cardQuestion
              );
              const submission = state.submissions
                ? state.submissions[0]
                : null;
              const response =
                submission && submission.response ? submission.response : "";
              const { maxScore = 1, minScore = 0 } = cardQuestion;
              const { options } = cardQuestion.content.question;
              const questionId = cardQuestion._id;
              const questionComplete = checkQuestionCompleted({
                currentQuestion: cardQuestion,
                currentQuestionState: state
              });
              const answerEvaluation = questionComplete
                ? evaluateSelectResponse(response, options)
                : "NA";
              const answer = {
                response,
                questionId,
                responseType: questionResponseType,
                timeStamp: new Date().toISOString(),
                progressMeasure: 1,
                answerEvaluation,
                rawScore: getRawScore({
                  answerEvaluation,
                  maxScore,
                  minScore,
                  penaltyMarks
                })
              };
              answersList.push(answer);
              break;
            }
            default:
              break;
          }
        });
        const mutationProps = {
          answers: answersList
        };
        return this.addQuestionSubmission(mutationProps);
      }
      case "START_TIMER_FOR_CARD": {
        const timeStamp = new Date().toISOString();
        const cardData = this.state.cardData;
        cardData.state = {
          ...cardData.state,
          startTimestamp: timeStamp,
          lastAccessedOn: timeStamp,
          updatedAt: timeStamp
        };
        this.setState({ cardData: { ...cardData } });
        break;
      }
      case "FLAG_QUESTION": {
        const { currentQuestion, currentQuestionState } = payload;
        const questionId = currentQuestion._id;
        const currentFlagged = !!currentQuestionState.flagged;
        const { flagged = currentFlagged } = payload;
        const cardData = this.state.cardData;
        cardData.card.children = this.state.cardData.card.children.map(card => {
          if (card.state.contentId === questionId) {
            const timeStamp = new Date().toISOString();
            return {
              cardQuestion: card.cardQuestion,
              state: {
                ...card.state,
                flagged,
                lastAccessedOn: timeStamp,
                lastSubmittedOn: timeStamp,
                updatedAt: timeStamp
              }
            };
          }
          return card;
        });

        this.setState({ cardData: { ...cardData } });
        break;
      }
      case "CHECK_ANSWER_CLICKED": {
        const { currentQuestion, currentQuestionState } = payload;
        const questionId = currentQuestion._id;
        const currentCheckAnswersAttemptCount =
          currentQuestionState.checkAnswersAttemptCount;
        const {
          checkAnswersAttemptCount = currentCheckAnswersAttemptCount
        } = payload;

        const cardData = this.state.cardData;
        cardData.card.children = this.state.cardData.card.children.map(card => {
          if (card.state.contentId === questionId) {
            const timeStamp = new Date().toISOString();
            return {
              cardQuestion: card.cardQuestion,
              state: {
                ...card.state,
                checkAnswersAttemptCount,
                lastAccessedOn: timeStamp,
                lastSubmittedOn: timeStamp,
                updatedAt: timeStamp
              }
            };
          }
          return card;
        });

        this.setState({ cardData: { ...cardData } });
        break;
      }
      case "UPDATE_RESPONSE": {
        const questionCardContainer = this.questionCardContainer;
        if (questionCardContainer && questionCardContainer.showLoader) {
          questionCardContainer.showLoader();
        }
        const { currentQuestion, currentQuestionState } = payload;
        const currentResponse =
          currentQuestionState.submissions &&
          currentQuestionState.submissions[0].response;
        const questionId = currentQuestion._id;
        const currentCheckAnswersAttemptCount =
          currentQuestionState.checkAnswersAttemptCount;
        const currentFlagged = !!currentQuestionState.flagged;
        const responseType = getObjectiveResponseType(currentQuestion);
        const {
          response = currentResponse || "",
          checkAnswersAttemptCount = currentCheckAnswersAttemptCount,
          flagged = currentFlagged
        } = payload;
        const isSubmit = false;
        const answerEvaluation = payload.answerEvaluation || "NA";
        const rawScore = payload.rawScore || 0;
        const progressMeasure = isSubmit ? 1 : 0;
        return this.perQuestionSubmission(
          cloneDeep({
            cardId: this.state.cardData.card._id,
            answerObject: {
              questionId,
              response,
              answerEvaluation,
              timeStamp: new Date().toISOString(),
              responseType,
              rawScore,
              progressMeasure
            },
            flagged,
            checkAnswersAttemptCount,
            actualSubmit: isSubmit
          })
        );
      }
      case "TRY_AGAIN_ASSIGNMENT": {
        window.location.reload();
        break;
      }
      case "NEXT_QUESTION_PRESSED": {
        break;
      }
      case "PREVIOUS_QUESTION_PRESSED": {
        break;
      }
      default:
        break;
    }
  };
  render() {
    return (
      <ThemeProvider theme={themeConfig}>
        <div className={s.appRoot}>
          <NewObjectiveAssessment
            ref={component =>
              !!component && (this.questionCardContainer = component)
            }
            cardData={props.cardData}
            dispatch={this.dispatch}
            isMobile={this.state.isMobile}
            isAssignmentFullScreen={false}
            updateAssignmentScreenStatus={
              this.props.updateAssignmentScreenStatus
            }
            menuOpen={this.props.menuOpen}
            cardListMenuBtn={this.props.cardListMenuBtn}
            theme={props.theme}
          />
        </div>
      </ThemeProvider>
    );
  }
}

export default App;
