import React, { Component } from "react";
import { css } from "emotion";
import Model from "./model";
import Header from "./model-views/Header";
import Question from "./model-views/Question";

const questionContainer = css`
  right: 0;
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  z-index: 50;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #fafafa;
`;

class NewObjectiveAssessment extends Component {
  UNSAFE_componentWillMount() {
    this.model = new Model();
    this.model.actions.syncStore(this.props);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.model.actions.syncStore(nextProps);
  }

  render() {
    const { questionsLength, isAssignmentFullScreen } = this.model.store;
    if (questionsLength === 0) {
      return <div>No Questions!</div>;
    }
    const mainClass = isAssignmentFullScreen ? questionContainer : "";
    return (
      <div className={mainClass}>
        {isAssignmentFullScreen && <Header model={this.model} />}
        <Question model={this.model} />
      </div>
    );
  }
}

export default NewObjectiveAssessment;
