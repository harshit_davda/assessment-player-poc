import React from 'react';
import styled from 'react-emotion';

const IconWrapper = styled.div`
  width: 35px;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 35px;
  cursor: pointer;
  flex-direction: column;
  border-radius: 4px;
  ${props => (props.isOpen
    ? `
    border: 1px solid ${props.theme.color.accent};
    background-color: #f5f9ff;
  `
    : `
    border: 1px solid ${props.theme.color.grey1};
  `)};
`;

const Line = styled.div`
  width: 13.1px;
  height: 1.5px;
  ${props => (props.isOpen
    ? `
    background-color: ${props.theme.color.accent};
  `
    : `
    background-color: ${props.theme.color.black2};
  `)};
`;

function AssessmentOptionsIcon({ theme, isOpen, onClick }) {
  return (
    <IconWrapper theme={theme} isOpen={isOpen} onClick={onClick}>
      <Line theme={theme} isOpen={isOpen} />
      <Line theme={theme} isOpen={isOpen} style={{ marginTop: '3.6px', marginBottom: '3.6px' }} />
      <Line theme={theme} isOpen={isOpen} />
    </IconWrapper>
  );
}

export default AssessmentOptionsIcon;
