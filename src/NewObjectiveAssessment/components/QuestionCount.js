import React from 'react';
import { css } from 'emotion';

const wrapper = theme => css`
  font-family: ProximaNova;
  font-size: 16px;
  font-weight: 600;
  color: ${theme.color.black2};
  margin-left: 10px;
`;

function QuestionCount({ theme, currentQuestionIndex, questionsLength }) {
  return (
    <div className={wrapper(theme)}>
      <span>Q. {currentQuestionIndex + 1}</span>
      <span style={{ fontWeight: 'normal' }}> of {questionsLength}</span>
    </div>
  );
}

export default QuestionCount;
