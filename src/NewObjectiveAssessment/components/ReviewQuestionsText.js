import React from 'react';
import { css } from 'emotion';

const wrapper = theme => css`
  font-family: ProximaNova;
  font-size: 16px;
  font-weight: 600;
  color: ${theme.color.black2};
  margin-left: 10px;
`;

function ReviewQuestionsText({ theme }) {
  return <div className={wrapper(theme)}>Review Questions</div>;
}

export default ReviewQuestionsText;
