import React from 'react';
import styled from 'react-emotion';

const GridContainer = styled.div`
  border-radius: 4px;
  height: 35px;
  width: 35px;
  padding: 8px 7px;
  display: flex;
  flex-wrap: wrap;
  cursor: pointer;
  ${props => (props.selected
    ? `
    border: 1px solid ${props.theme.color.accent};
    background-color: #f5f9ff;
  `
    : `
    border: 1px solid ${props.theme.color.grey1};
  `)};
`;

const GridElement = styled.div`
  width: 4px;
  height: 4px;
  margin: 1px;
  border-radius: 1px;
  ${props => (props.selected
    ? `
    background-color: ${props.theme.color.accent};
  `
    : `
    background-color: ${props.theme.color.black3};
  `)};
`;

function GridIcon({ theme, selected, onGridClick }) {
  return (
    <GridContainer theme={theme} selected={selected} onClick={onGridClick}>
      <GridElement theme={theme} selected={selected} />
      <GridElement theme={theme} selected={selected} />
      <GridElement theme={theme} selected={selected} />
      <GridElement theme={theme} selected={selected} />
      <GridElement theme={theme} selected={selected} />
      <GridElement theme={theme} selected={selected} />
      <GridElement theme={theme} selected={selected} />
      <GridElement theme={theme} selected={selected} />
    </GridContainer>
  );
}

export default GridIcon;
