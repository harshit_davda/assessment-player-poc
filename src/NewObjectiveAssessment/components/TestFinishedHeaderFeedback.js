import React from 'react';
import styled from 'react-emotion';

const Wrapper = styled.div`
  font-family: ProximaNova;
  width: 100%;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  ${props => `
    color: ${props.theme.color.black1};
  `};
  ${({ isMobile }) => `
  font-size: ${isMobile ? '22px' : '34px'};
  `}
`;

function ScoreCardHeaderFeedback({ theme, passed, isMobile, isGradable }) {
  return (
    <Wrapper theme={theme} isMobile={isMobile}>
      {isGradable
        ? passed
          ? 'Congratulations, Great Work!'
          : 'Sorry, you didn’t pass.'
        : 'Assessment is Complete!'}
    </Wrapper>
  );
}

export default ScoreCardHeaderFeedback;
