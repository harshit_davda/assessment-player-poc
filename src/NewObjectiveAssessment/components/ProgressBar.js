import React, { PureComponent } from "react";
import ProgressBar from "../../components/ProgressBar";

const progressbarContainer = {
  width: "100%"
};
const progressBarBg = {
  backgroundColor: "#eaf0f8",
  height: "3px",
  borderRadius: "0px"
};
const progressBarFillStyle = {
  height: "3px",
  borderRadius: "100px"
};
const fillColor = {
  backgroundColor: "#4a90e2"
};

class QuestionProgressBar extends PureComponent {
  render() {
    const { progressPercent } = this.props;
    return (
      <ProgressBar
        progressPercent={progressPercent}
        progressBarContainerStyle={progressbarContainer}
        progressBarStyle={progressBarBg}
        progressBarFillStyle={progressBarFillStyle}
        progressBarFillColor={fillColor}
      />
    );
  }
}

export default QuestionProgressBar;
