import React from 'react';
import styled from 'react-emotion';
import { css } from 'emotion';

const normalText = css({
  fontFamily: 'ProximaNova',
  fontWeight: 'normal',
  fontStyle: 'normal',
  fontStretch: 'normal',
  lineHeight: 'normal',
  letterSpacing: 'normal'
});

const FeedBackComponentDiv = styled.div(
  {
    borderRadius: 8,
    backgroundColor: '#f5f9ff',
    padding: 15,
    position: 'relative',
    bottom: 0
  },
  props => ({
    width: props.isMobile ? 'calc(100% - 40px)' : 'calc(100% - 100px)',
    margin: props.isMobile ? '20px 20px 0' : '50px 50px 0'
  })
);

const FeedBackHeader = styled.div(
  normalText,
  {
    fontSize: 12,
    fontWeight: 600,
    letterSpacing: 0.9,
    textAlign: 'left',
    marginBottom: 6
  },
  props => ({
    color: props.color.black3
  })
);

const FeedBackBody = styled.div(normalText, props => ({
  color: props.color.black1
}));

export default class FeedBackComponent extends React.Component {
  render() {
    return (
      <FeedBackComponentDiv isMobile={this.props.isMobile}>
        <FeedBackHeader color={this.props.color}>{this.props.heading}</FeedBackHeader>
        <FeedBackBody
          color={this.props.color}
          dangerouslySetInnerHTML={{
            __html: this.props.body
          }}
        />
      </FeedBackComponentDiv>
    );
  }
}
