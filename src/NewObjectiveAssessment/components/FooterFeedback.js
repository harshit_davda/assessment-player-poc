import React from 'react';
import { Icon } from '@lyearn-team/frontend-components';
import styled from 'react-emotion';

const FeedbackContainer = styled.div`
  border-radius: 4px;
  padding: 12px 29px;
  display: flex;
  align-items: center;
  font-family: ProximaNova;
  font-size: 16px;
  font-weight: 600;
  font-style: normal;
  ${props => (props.isCorrect
    ? `
    background-color: #f5fff6;
    color: #39b54a;
  `
    : `
    background-color: #fff5f5;
    color: #f85359;
  `)};
  ${props => (props.isMobile
    ? `
    width: 185px;
    min-height: 40px;
    padding: 8px 14px;
    margin: auto;
    margin-top: 20px;
  `
    : `
    position: absolute;
    left: 50%;
    transform: translateX(-50%);
  `)};
`;

function CorrectAnswerFeedback() {
  return (
    <React.Fragment>
      <div
        style={{
          marginRight: '8px',
          width: '24px',
          height: '24px',
          display: 'flex',
          border: '1px solid #39b54a',
          alignItems: 'center',
          justifyContent: 'center',
          borderRadius: '50%'
        }}
      >
        <Icon type="checkGreen" />
      </div>
      <div>Correct Answer</div>
    </React.Fragment>
  );
}

function IncorrectAnswerFeedback() {
  return (
    <React.Fragment>
      <div style={{ width: '24px', height: '24px', marginRight: '8px' }}>
        <Icon type="rounded-cross" style={{ width: '24px', height: '24px', fill: '#f85359' }} />
      </div>
      <div>Incorrect Answer</div>
    </React.Fragment>
  );
}

function FooterFeedback({ isCorrect, isMobile = true }) {
  return (
    <FeedbackContainer isCorrect={isCorrect} isMobile={isMobile}>
      {isCorrect ? <CorrectAnswerFeedback /> : <IncorrectAnswerFeedback />}
    </FeedbackContainer>
  );
}

export default FooterFeedback;
