import snakeCase from 'lodash/snakeCase';
import $j from 'jquery';

const getObjectiveResponseType = questionOffer => snakeCase(questionOffer.content.question.responseType[0]).toUpperCase();

const getCorrectFTBMappedObj = (ftbComponents) => {
  const correctMappedObj = {};
  ftbComponents
    .filter(item => item.isBlank)
    .forEach((item) => {
      correctMappedObj[item._id] = item._id;
    });
  return correctMappedObj;
};
const getFTBSelectedIds = (currentQuestionState) => {
  const latestResponse = currentQuestionState.submissions
    ? currentQuestionState.submissions[0].response || '{}'
    : '{}';
  let mappedClickedObj = {};
  if (latestResponse) {
    try {
      mappedClickedObj = JSON.parse(latestResponse);
    } catch (e) {
      mappedClickedObj = {};
    }
  }
  return mappedClickedObj;
};

const getMTFLeftRightOptions = (pairs) => {
  const leftOptions = pairs.map(item => item.left);
  const rightOptions = pairs.map(item => item.right);
  rightOptions.forEach((item) => {
    const labelText = '<div>' + item.text + '</div>';
    item.value = item.text;
    try {
      item.label = $j(labelText).text();
    } catch (e) {
      item.label = item.text;
    }
  });
  return { leftOptions, rightOptions };
};
const getMTFSelectedLeftOptions = (currentQuestionState) => {
  const latestResponse = currentQuestionState.submissions
    ? currentQuestionState.submissions[0].response || '[]'
    : '[]';
  const responseArr = JSON.parse(latestResponse);
  const selectedLeftOptions = {};
  responseArr.forEach((item) => {
    selectedLeftOptions[item.leftId] = item.rightId || '';
  });
  return selectedLeftOptions;
};

const checkQuestionCompleted = ({ currentQuestion, currentQuestionState }) => {
  const questionType = getObjectiveResponseType(currentQuestion);
  let questionCompleted = false;
  switch (questionType) {
    case 'FILL_IN_THE_BLANKS': {
      const ftbComponents = currentQuestion.content.question.ftbComponents;
      const selectedIds = getFTBSelectedIds(currentQuestionState);
      const correctMappedObj = getCorrectFTBMappedObj(ftbComponents);
      questionCompleted = Object.keys(selectedIds).length === Object.keys(correctMappedObj).length
        && Object.values(selectedIds).indexOf('') === -1;
      break;
    }
    case 'MATCH_THE_FOLLOWING': {
      const pairs = currentQuestion.content.question.pairs;
      const { leftOptions } = getMTFLeftRightOptions(pairs);
      const selectedLeftOptions = getMTFSelectedLeftOptions(currentQuestionState);
      questionCompleted = leftOptions.length === Object.keys(selectedLeftOptions).length
        && Object.values(selectedLeftOptions).indexOf('') === -1;
      break;
    }
    case 'SINGLE_SELECT':
    case 'MULTIPLE_SELECT':
      questionCompleted = currentQuestionState.submissions
        && currentQuestionState.submissions[0].response
        && currentQuestionState.submissions[0].response.length;
      break;
    default:
      questionCompleted = false;
  }
  return questionCompleted;
};

const marksUptoTwoDecimal = marks => Number(parseInt(marks || 0, 10).toFixed(2));

export {
  getObjectiveResponseType,
  getCorrectFTBMappedObj,
  checkQuestionCompleted,
  getFTBSelectedIds,
  getMTFLeftRightOptions,
  getMTFSelectedLeftOptions,
  marksUptoTwoDecimal
};
