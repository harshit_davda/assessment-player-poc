import { action } from 'mobx';
import { SnackBarHelper } from '@lyearn-team/frontend-components';
import cloneDeep from 'lodash/cloneDeep';
import { checkQuestionCompleted } from '../../helpers';

class Actions {
  constructor(store) {
    this.store = store;
  }

  showStatus(label, status) {
    SnackBarHelper.showSnackbar({
      label,
      status
    });
  }

  @action
  showScoreCard = () => {
    this.store.showScoreCard = true;
  };

  @action
  hideScoreCard = () => {
    this.store.showScoreCard = false;
  };

  @action
  setTestFinished = () => {
    this.store.testFinished = true;
  };

  @action
  setTimeLeft = () => {
    const { card, cardState } = this.store;
    const timeTocomplete = card.content.duration * 1000;
    const startISODate = cardState.startTimestamp;
    const startAt = new Date(startISODate).getTime();
    const endTime = startAt + timeTocomplete;
    const currentTime = new Date().getTime();
    const timeLeft = endTime - currentTime;
    this.store.timeLeft = timeLeft;
  };

  @action
  resetFlags(obj) {
    const defaultObj = {
      checkAnswer: false,
      answerStatus: null,
      seeCorrectAnswers: false,
      seeActualAnswers: false
    };
    let resetObj = obj || {};
    resetObj = { ...defaultObj, ...resetObj };
    const resetObjArr = Object.keys(resetObj);
    resetObjArr.forEach(item => {
      this.store[item] = resetObj[item];
    });
  }

  @action
  syncStore(props) {
    this.store.syncProps(props);
  }

  @action
  toggleGridView = from => {
    const { gridView } = this.store;
    this.hideScoreCard();
    window.analytics &&
      window.analytics.track('OBJECTIVE_ASSIGNMENT_GRID_VIEW_CLICKED', {
        url: window.location.href,
        open: !this.store.gridView,
        from
      });
    this.store.gridView = !gridView;
    this.store.assessmentOptionsOpen = false;
    this.resetFlags();
  };

  @action
  startTest = async () => {
    const { dispatch, card } = this.store;
    if (card.timeBoundTest || card.isGradable) {
      try {
        await dispatch('START_TIMER_FOR_CARD');
        if (this.store.isPreviewMode) {
          this.store.showTestStartScreen = false;
        }
        // this.store.showTestStartScreen = false;
        this.store.unAnsweredQuestionLength = 0;
      } catch (err) {
        this.showStatus('Some error occured. Please try again!', 'error');
      }
    }
    this.resetFlags();
  };

  @action
  backToScoreCard = () => {
    this.store.gridView = false;
    this.showScoreCard();
  };

  @action
  finishTest = async finishConfirmButtonFlag => {
    const { card, cardState, dispatch, testFinished, originalSequence } = this.store;
    let unAnsweredQuestionLength = 0;
    if (card.randomizeQuestion) {
      const { children } = card;
      const originalMappedArr = [];
      const childrenLen = children.length;
      for (let i = 0; i < childrenLen; i += 1) {
        originalMappedArr.push({});
      }
      children.forEach(item => {
        const originalIndex = originalSequence.indexOf(item.cardQuestion._id);
        originalMappedArr[originalIndex] = item;
      });
      card.children = cloneDeep(originalMappedArr);
    }
    const isFinished = card.children.every(question => {
      const questionComplete = checkQuestionCompleted({
        currentQuestion: question.cardQuestion,
        currentQuestionState: question.state
      });
      return questionComplete;
    });
    card.children.forEach(question => {
      const questionComplete = checkQuestionCompleted({
        currentQuestion: question.cardQuestion,
        currentQuestionState: question.state
      });
      if (!questionComplete) {
        unAnsweredQuestionLength += 1;
      }
    });
    if ((!testFinished && !finishConfirmButtonFlag) || (isFinished && finishConfirmButtonFlag)) {
      try {
        await dispatch('FINISH_TEST', { card, cardState });
        this.showScoreCard();
        this.setTestFinished();
        this.store.gridView = false;
        this.resetFlags({
          checkAnswer: false
        });
        this.store.timeLeft = null;
      } catch (err) {
        this.showStatus('Some error occured. Please try again!', 'error');
      }
    } else if (!testFinished && finishConfirmButtonFlag) {
      this.store.showUnAttemptedPopup = true;
      this.store.unAnsweredQuestionLength = unAnsweredQuestionLength;
    } else if (isFinished) {
      this.store.timeLeft = null;
      this.showScoreCard();
    }
  };

  @action
  onCloseAnAttemptedPopup = () => {
    this.store.showUnAttemptedPopup = false;
  };

  @action
  onRetryTestClicked = async () => {
    const { card, cardState, dispatch } = this.store;
    try {
      await dispatch('TRY_AGAIN_ASSIGNMENT', { card, cardState });
      this.store.retryTestInvoked = true;
      this.hideScoreCard();
      this.goToQuestion(0);
    } catch (err) {
      this.showStatus('Some error occured. Please try again!', 'error');
    }
  };

  @action
  updateAnswerStatus = status => {
    const questionComplete = checkQuestionCompleted({
      currentQuestion: this.store.currentQuestion,
      currentQuestionState: this.store.currentQuestionState
    });
    if (questionComplete && this.store.testFinished) {
      this.store.answerStatus = status;
    }
  };

  @action
  disableCheckBtnStatus = status => {
    this.store.disableCheckBtn = status;
  };

  @action
  checkAnswerClicked = async () => {
    const { currentQuestion, currentQuestionState, dispatch } = this.store;
    const checkAnswersAttemptCount =
      typeof currentQuestionState.checkAnswersAttemptCount === 'number'
        ? (currentQuestionState.checkAnswersAttemptCount += 1)
        : 1;
    const responseObj = {
      currentQuestion,
      currentQuestionState,
      checkAnswersAttemptCount
    };
    try {
      await dispatch('CHECK_ANSWER_CLICKED', responseObj);
      this.store.checkAnswer = !this.store.checkAnswer;
      this.store.checkAnswer && this.updateAnswerStatus('correct');
    } catch (err) {
      this.showStatus('Some error occured. Please try again!', 'error');
    }
  };

  @action
  revealCorrectAnswers = () => {
    this.resetFlags();
    this.store.seeCorrectAnswers = true;
  };

  @action
  yourResponseClick = () => {
    this.resetFlags();
    this.store.seeCorrectAnswers = false;
    this.store.seeActualAnswers = true;
  };

  @action
  toggleAssessmentOptions = () => {
    const { assessmentOptionsOpen } = this.store;
    this.store.assessmentOptionsOpen = !assessmentOptionsOpen;
    this.resetFlags();
  };

  @action
  goToQuestion = index => {
    const { questionsLength } = this.store;
    this.store.gridView = false;
    if (index < 0 || index > questionsLength) {
      return;
    }
    this.store.currentQuestionIndex = index;
    this.resetFlags();
    this.store.revealOnTestFinish && this.updateAnswerStatus('correct');
  };

  @action
  goToNextQuestion = () => {
    const { currentQuestionIndex, dispatch } = this.store;
    const previousQuestionData = {
      cardQuestion: this.store.currentQuestion,
      state: this.store.currentQuestionState
    };
    this.goToQuestion(currentQuestionIndex + 1);
    const currentQuestionData = {
      cardQuestion: this.store.currentQuestion,
      state: this.store.currentQuestionState
    };
    this.resetFlags();
    this.store.revealOnTestFinish && this.updateAnswerStatus('correct');
    dispatch('NEXT_BUTTON_PRESSED', { previousQuestionData, currentQuestionData });
  };

  @action
  goToPreviousQuestion = () => {
    const { currentQuestionIndex, dispatch } = this.store;
    const previousQuestionData = {
      cardQuestion: this.store.currentQuestion,
      state: this.store.currentQuestionState
    };
    this.goToQuestion(currentQuestionIndex - 1);
    const currentQuestionData = {
      cardQuestion: this.store.currentQuestion,
      state: this.store.currentQuestionState
    };
    this.resetFlags();
    this.store.revealOnTestFinish && this.updateAnswerStatus('correct');
    dispatch('PREVIOUS_BUTTON_PRESSED', { previousQuestionData, currentQuestionData });
  };

  @action
  updateSelectedOptions = selectedOptions => {
    const optionsSelected = selectedOptions;
    let response = '';
    optionsSelected.forEach((item, index) => {
      if (item) {
        if (!response) {
          response += index.toString();
        } else {
          response += `,${index.toString()}`;
        }
      }
    });
    this.resetFlags();
    this.dispatchUpdateResponse({ response });
  };

  @action
  dispatchUpdateResponse = async ({ response }) => {
    const { currentQuestion, currentQuestionState, dispatch } = this.store;
    try {
      await dispatch('UPDATE_RESPONSE', {
        currentQuestion,
        currentQuestionState,
        response
      });
      this.resetFlags();
    } catch (err) {
      this.showStatus('Some error occured. Please try again!', 'error');
    }
  };

  @action
  flagQuestion = async () => {
    // this.store.checkAnswer = !this.store.checkAnswer;
    // this.store.checkAnswer && this.updateAnswerStatus('correct');
    const { currentQuestion, currentQuestionState, testFinished, dispatch } = this.store;
    if (!testFinished) {
      window.analytics &&
        window.analytics.track('OBJECTIVE_ASSIGNMENT_QUESTION_FLAG_CLICKED', {
          url: window.location.href,
          flagged: !currentQuestionState.flagged
        });
      const flagged = !currentQuestionState.flagged;
      const responseObj = {
        currentQuestion,
        currentQuestionState,
        flagged
      };
      try {
        await dispatch('FLAG_QUESTION', responseObj);
        this.toggleAssessmentOptions();
        this.resetFlags();
      } catch (err) {
        this.showStatus('Some error occured. Please try again!', 'error');
      }
    }
  };

  @action
  updateMTFOptions = responseString => {
    const responseObj = {
      response: responseString
    };
    this.resetFlags();
    this.dispatchUpdateResponse(responseObj);
  };

  @action
  updateFtbOptions = ({ mappedClickedObj }) => {
    const responseString = JSON.stringify(mappedClickedObj);
    const responseObj = {
      response: responseString
    };
    this.resetFlags();
    this.dispatchUpdateResponse(responseObj);
  };
}

export default Actions;
