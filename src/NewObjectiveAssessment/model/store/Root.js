import { observable, action, computed } from "mobx";
import cloneDeep from "lodash/cloneDeep";
import { knuthShuffle } from "knuth-shuffle";

class RootStore {
  dispatch;
  theme;
  updateAssignmentScreenStatus;
  cardListMenuBtn;
  menuOpen;
  shuffledSequence = [];
  @observable.ref card;
  @observable.ref cardState;
  @observable currentQuestionIndex = 0;
  @observable gridView = false;
  @observable assessmentOptionsOpen = false;
  @observable isMobile = false;
  @observable testFinished = false;
  @observable showScoreCard = false;
  @observable showTestStartScreen = true;
  @observable showUnAttemptedPopup = false;
  @observable unAnsweredQuestionLength = 0;
  @observable checkAnswer = false;
  @observable seeCorrectAnswers = false;
  @observable answerStatus = null;
  @observable disableCheckBtn = true;
  @observable isAssignmentFullScreen = false;
  @observable syncFirstTime = true;
  // @observable headerText = '';
  @observable seeActualAnswers = false;
  @observable originalSequence = [];
  @observable timeLeft = null;
  @observable isGradable = false;
  @observable retryTestInvoked = false;
  @observable reAttemptsLeft = 0;
  @observable isPreviewMode = false;

  getShuffledOrder = children => {
    if (this.shuffledSequence.length) {
      return this.shuffledSequence;
    }
    children.forEach(item => {
      this.originalSequence.push(item.cardQuestion._id);
    });
    const shuffledChildren = knuthShuffle(children);
    shuffledChildren.forEach(item => {
      this.shuffledSequence.push(item.cardQuestion._id);
    });
    return this.shuffledSequence;
  };

  shuffleChildren = children => {
    if (this.retryTestInvoked) {
      this.shuffledSequence = [];
      this.originalSequence = [];
      this.retryTestInvoked = false;
    }
    return this.getShuffledOrder(children);
  };

  @action
  shuffledChildren(card) {
    const clonedCard = cloneDeep(card);
    const { children } = clonedCard;
    const shuffledSeq = this.shuffleChildren(children);
    const arrLen = children.length;
    const newMappedArr = [];
    for (let i = 0; i < arrLen; i += 1) {
      newMappedArr.push({});
    }
    children.forEach(item => {
      const shuffledIndex = shuffledSeq.indexOf(item.cardQuestion._id);
      newMappedArr[shuffledIndex] = item;
    });
    clonedCard.children = newMappedArr;
    return cloneDeep(clonedCard);
  }

  @action
  syncProps(props) {
    const clonedProps = cloneDeep(props);
    const {
      cardData,
      dispatch,
      isMobile,
      theme,
      isAssignmentFullScreen,
      updateAssignmentScreenStatus,
      cardListMenuBtn,
      menuOpen,
      preview
    } = clonedProps;
    this.isPreviewMode = preview;
    const { reAttemptAllowedCount } = cardData.card;
    const { reAttemptCount } = cardData.state;
    if (reAttemptAllowedCount === -1) {
      this.reAttemptsLeft = 1;
    } else {
      this.reAttemptsLeft = 1 + (reAttemptAllowedCount - reAttemptCount);
    }
    this.card = cardData.card.randomizeQuestions
      ? this.shuffledChildren(cardData.card)
      : cardData.card;
    this.cardState = cardData.state;
    this.isGradable = this.card.isGradable;
    this.dispatch = dispatch;
    this.theme = theme;
    this.isMobile = isMobile;
    this.isAssignmentFullScreen = isAssignmentFullScreen;
    this.updateAssignmentScreenStatus = updateAssignmentScreenStatus;
    this.cardListMenuBtn = cardListMenuBtn;
    this.menuOpen = menuOpen;
    this.testFinished = cardData.state.actualSubmit || this.reAttemptsLeft <= 0;
    this.showScoreCard = this.syncFirstTime
      ? this.testFinished
      : this.showScoreCard;
    this.showTestStartScreen = this.testFinished
      ? false
      : (cardData.card.timeBoundTest || this.isGradable) &&
        !cardData.state.startTimestamp;
    this.syncFirstTime = false;
    // const { card, cardState } = this;
    // const timeTocomplete = card.content.duration * 1000;
    // const startISODate = cardState.startTimestamp;
    // const startAt = new Date(startISODate).getTime();
    // const endTime = startAt + timeTocomplete;
    // const currentTime = new Date().getTime();
    // const timeLeft = endTime - currentTime;
    // this.timeLeft = timeLeft;
  }

  @computed
  get currentQuestion() {
    return this.card.children[this.currentQuestionIndex].cardQuestion;
  }

  @computed
  get currentQuestionState() {
    return this.card.children[this.currentQuestionIndex].state;
  }

  @computed
  get questionsLength() {
    return this.card.children.length;
  }

  @computed
  get answerAttempCount() {
    return (
      this.card.children[this.currentQuestionIndex].state
        .checkAnswersAttemptCount || 0
    );
  }

  @computed
  get revealCorrectOnCheckAnswer() {
    return (
      this.card.questionSettings &&
      this.card.questionSettings.revealCorrectOnCheckAnswer
    );
  }

  @computed
  get showCheckAnswerBtn() {
    return (
      this.card.questionSettings &&
      this.card.questionSettings.allowCheckAnswer &&
      this.card.questionSettings.answerAttemptsAllowed > this.answerAttempCount
    );
  }

  @computed
  get penaltyMarks() {
    return this.isGradable
      ? this.card.questionSettings
        ? this.card.questionSettings.penaltyMarks || 0
        : 0
      : 0;
  }

  @computed
  get revealOnTestFinish() {
    let flagStr = "";
    if (this.testFinished) {
      if (
        typeof this.card.reviewQuestionsEnabled === "boolean" &&
        !this.card.reviewQuestionsEnabled
      ) {
        return "";
      }
      if (typeof this.card.reviewQuestionsEnabled === "boolean") {
        flagStr = this.card.revealCorrectAnswer ? "REVEAL" : "DONT_REVEAL";
        return flagStr;
      }
      flagStr = this.card.content.hideAnswers ? "DONT_REVEAL" : "REVEAL";
    }
    return flagStr;
  }

  @computed
  get revealCorrectAnsGrid() {
    let flagStr = false;
    if (this.testFinished) {
      if (
        typeof this.card.reviewQuestionsEnabled === "boolean" &&
        this.card.reviewQuestionsEnabled
      ) {
        return true;
      }
      if (
        typeof this.card.reviewQuestionsEnabled === "boolean" &&
        !this.card.reviewQuestionsEnabled
      ) {
        return false;
      }
      flagStr = true;
    }
    return flagStr;
  }

  @computed
  get reAttemptAllowed() {
    return this.card.reAttemptAllowed;
  }

  @computed
  get reAttemptAllowedCount() {
    return this.card.reAttemptAllowed ? this.card.reAttemptAllowedCount : 0;
  }

  @computed
  get progressPercent() {
    return ((this.currentQuestionIndex + 1) / this.questionsLength) * 100;
  }
}

export default RootStore;
