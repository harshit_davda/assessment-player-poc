/* eslint-disable import/no-extraneous-dependencies */
import styled from '@emotion/styled';

const normalText = {
  fontFamily: 'ProximaNova',
  fontWeight: 'normal',
  fontStyle: 'normal',
  fontStretch: 'normal',
  lineHeight: 'normal',
  letterSpacing: 'normal'
};
const LightTextComponent = styled.span(
  {
    fontSize: 16
  },
  normalText
);
const NormalTextComponent = styled.span(
  normalText,
  {
    fontSize: 19,
    lineHeight: 2.5
  },
  props => ({
    color: props.color.black1,
    [LightTextComponent]: {
      color: props.color.black2
    }
  })
);
const FTBDropDownText = styled.span({
  fontSize: 14,
  lineHeight: 1.43
});
const FtbDiv = styled.div`
  padding: 20px 50px;
  display: flex;
  flex-wrap: wrap;
  ${({ isMobile }) => `
    padding: ${isMobile ? '0 20px' : '0 50px'};
  `}
`;
const borderColorObj = {
  correct: 'green',
  wrong: 'red',
  selected: 'accent',
  default: 'grey1'
};
const getSelectStyle = (theme, isMobile) => {
  const styleStr = `
    .Select {
      display: inline-block;
      margin: ${isMobile ? '10px' : '5px 10px'};
      width: auto;
      min-width: 145px;
      height: 40px;
      border-radius: 4px;
      border: 1px solid ${theme.color[borderColorObj.default]};
    }
    .Select div[role="button"]::after,
    .Select div[role="button"]::before {
      transition: none;
    }
    .Select div[role="button"]:hover::before {
      border-bottom: none !important;
    }
    .Select div[role="button"]:focus::after,
    .Select div[role="button"]:active::after {
      border-bottom: none !important;
    }
    .correct .Select {
      border: 1px solid ${theme.color[borderColorObj.correct]};
    }
    .wrong .Select {
      border: 1px solid ${theme.color[borderColorObj.wrong]};
    }
    .selected .Select {
      border: 1px solid ${theme.color[borderColorObj.selected]};
    }
    .Select > div {
      border: none;
    }
    .Select div[role="button"] {
      min-height: 38px;
      border: none;
      color: ${theme.color[borderColorObj.default]};
      padding: 0 10px;
      border-radius: 10px;
    }
    .wrong .Select div[role="button"] {
      color: ${theme.color[borderColorObj.wrong]};
    }
    .correct .Select div[role="button"] {
      color: ${theme.color[borderColorObj.correct]};
    }
    .selected .Select div[role="button"] {
      color: ${theme.color[borderColorObj.selected]};
    }
    .Select .Icon {
      color: ${theme.color[borderColorObj.default]};
      fill: ${theme.color[borderColorObj.default]};
    }
    .wrong .Select .Icon {
      color: ${theme.color[borderColorObj.wrong]};
      fill: ${theme.color[borderColorObj.wrong]};
    }
    .correct .Select .Icon {
      color: ${theme.color[borderColorObj.correct]};
      fill: ${theme.color[borderColorObj.correct]};
    }
    .selected .Select .Icon {
      color: ${theme.color[borderColorObj.selected]};
      fill: ${theme.color[borderColorObj.selected]};
    }
    .clear-text-drop-down {
      color: ${theme.color.red};
      padding-top: 2px;
      display: flex;
    }
    .Select button:last-child {
      margin-bottom: -8px;
      border-top: 1px solid ${theme.color.grey1};
    }
  `;
  return styleStr;
};

export { LightTextComponent, NormalTextComponent, FtbDiv, getSelectStyle, FTBDropDownText };
