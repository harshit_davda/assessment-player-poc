import React from "react";
import { observer } from "mobx-react";
import { Select } from "@lyearn-team/frontend-components";
import { knuthShuffle } from "knuth-shuffle";
import cloneDeep from "lodash/cloneDeep";
import difference from "lodash/difference";
import {
  LightTextComponent,
  NormalTextComponent,
  FtbDiv,
  getSelectStyle,
  FTBDropDownText
} from "./style";
import {
  getCorrectFTBMappedObj,
  checkQuestionCompleted,
  getFTBSelectedIds
} from "../../helpers";

@observer
class FillInTheBlanks extends React.Component {
  constructor(props) {
    super(props);
    const { currentQuestionState } = props.model.store;
    this.isDropDownShuffled = false;
    this.shuffledDropDownArr = [];
    this.shuffledDropDown = [];
    this.state = {
      selectedIds: getFTBSelectedIds(currentQuestionState)
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { currentQuestionState } = nextProps.model.store;
    const selectedIds = getFTBSelectedIds(currentQuestionState);
    this.setState({ selectedIds });
  }

  selected = correctId => clickedObj => {
    const { testFinished } = this.props.model.store;
    if (!testFinished) {
      const { selectedIds } = this.state;
      if (clickedObj.selectedId === "clearSelection_id") {
        selectedIds[correctId] = "";
      } else {
        const selectedIdsArr = Object.keys(selectedIds);
        selectedIdsArr.forEach(item => {
          if (selectedIds[item] === clickedObj.selectedId) {
            selectedIds[item] = "";
          }
        });
        selectedIds[correctId] = clickedObj.selectedId;
      }
      this.setState(
        {
          selectedIds
        },
        () => {
          const responseObj = {
            mappedClickedObj: selectedIds
          };
          this.props.model.actions.updateFtbOptions(cloneDeep(responseObj));
        }
      );
    }
  };

  getDropDownOptions = (ftbComponents, globalDistractors) => {
    const dropDownOption = [];
    ftbComponents.forEach(currComp => {
      if (currComp.isBlank) {
        dropDownOption.push({
          value: currComp.text,
          label: currComp.text,
          selectedId: currComp._id
        });
      }
    });
    globalDistractors.forEach(currComp => {
      dropDownOption.push({
        value: currComp.text,
        label: currComp.text,
        selectedId: currComp._id
      });
    });
    return cloneDeep(dropDownOption);
  };

  shuffleDropDownOptions = dropDownOption => {
    if (this.isDropDownShuffled) {
      const newDropDownArr = [];
      dropDownOption.forEach(item => {
        newDropDownArr.push(item.selectedId);
      });
      const isDropDownChanged = difference(
        newDropDownArr,
        this.shuffledDropDownArr
      );
      return isDropDownChanged.length
        ? knuthShuffle(dropDownOption)
        : this.shuffledDropDown;
    }
    const shuffledDropDown = knuthShuffle(dropDownOption);
    shuffledDropDown.forEach(item => {
      this.shuffledDropDownArr.push(item.selectedId);
    });
    this.shuffledDropDown = shuffledDropDown;
    this.isDropDownShuffled = true;
    return shuffledDropDown;
  };

  selectCustomOptions = ({ option: { value } }) => (
    <FTBDropDownText
      dangerouslySetInnerHTML={{
        __html: value
      }}
    />
  );

  render() {
    const {
      isMobile,
      checkAnswer,
      seeCorrectAnswers,
      testFinished,
      revealOnTestFinish,
      currentQuestion,
      currentQuestionState,
      isGradable
    } = this.props.model.store;
    const { maxScore, content } = currentQuestion;
    const { ftbComponents, globalDistractors } = content.question;
    let dropDownOption = this.getDropDownOptions(
      ftbComponents,
      globalDistractors
    );
    dropDownOption = this.shuffleDropDownOptions(dropDownOption);
    const {
      updateAnswerStatus,
      disableCheckBtnStatus
    } = this.props.model.actions;
    let newMappedClickedObj = {
      ...this.state.selectedIds
    };
    const correctMappedObj = getCorrectFTBMappedObj(ftbComponents);
    const questionComplete = checkQuestionCompleted({
      currentQuestion,
      currentQuestionState
    });
    disableCheckBtnStatus(!questionComplete);
    !questionComplete &&
      (checkAnswer || revealOnTestFinish) &&
      updateAnswerStatus("wrong");
    if (seeCorrectAnswers) {
      newMappedClickedObj = cloneDeep(correctMappedObj);
    }
    return (
      <FtbDiv isMobile={isMobile}>
        {ftbComponents.map(currComp => {
          if (currComp.isBlank) {
            const correctId = currComp._id;
            let newDropDownOption = cloneDeep(dropDownOption);
            newMappedClickedObj = newMappedClickedObj || {};
            const selectedValues = newDropDownOption.filter(
              item => item.selectedId === newMappedClickedObj[correctId]
            );
            newDropDownOption = newDropDownOption.filter(
              itm =>
                Object.values(this.state.selectedIds).indexOf(
                  itm.selectedId
                ) === -1
            );
            let selectionStatus = null;
            selectionStatus = selectedValues.length
              ? "selected"
              : selectionStatus;
            if (
              (checkAnswer || revealOnTestFinish) &&
              this.state.selectedIds[correctId]
            ) {
              selectionStatus =
                this.state.selectedIds[correctId] === correctId
                  ? "correct"
                  : "wrong";
              selectionStatus === "wrong" && updateAnswerStatus("wrong");
            }
            const clearBtnProps = {
              label: "",
              selectedId: "clearSelection_id"
            };
            selectionStatus = seeCorrectAnswers ? "correct" : selectionStatus;
            return (
              <span className={selectionStatus}>
                <style>
                  {getSelectStyle(this.props.model.store.theme, isMobile)}
                </style>
                <Select
                  onSelect={this.selected(correctId)}
                  label="Select"
                  options={newDropDownOption}
                  minWidth={100}
                  selectedOptions={selectedValues}
                  disabled={testFinished}
                  customOption={this.selectCustomOptions}
                  showClearBtn={true}
                  clearBtnProps={clearBtnProps}
                />
              </span>
            );
          }
          return (
            <NormalTextComponent
              color={this.props.model.store.theme.color}
              dangerouslySetInnerHTML={{
                __html: currComp.text
              }}
            />
          );
        })}
      </FtbDiv>
    );
  }
}

export default FillInTheBlanks;
