/* eslint-disable import/no-extraneous-dependencies */
import styled from '@emotion/styled';
import { css } from '@emotion/core';

const normalText = css({
  fontFamily: 'ProximaNova',
  fontWeight: 'normal',
  fontStyle: 'normal',
  fontStretch: 'normal',
  lineHeight: 'normal',
  letterSpacing: 'normal'
});

const LightTextComponent = styled.span(
  {
    fontSize: 16
  },
  normalText
);

const NormalTextComponent = styled.span(
  {
    fontSize: 19
  },
  normalText,
  props => ({
    color: props.color.black1,
    fontSize: props.isMobile ? 18 : 19,
    [LightTextComponent]: {
      color: props.color.black2
    }
  })
);

const mtfTabTextStyle = css(
  {
    fontSize: 16,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  normalText
);

const MatchTheFollowingTabsText = styled.span(mtfTabTextStyle, {
  fontSize: 14,
  lineHeight: 1.43
});

const LeftSectionBox = styled.span(
  {
    width: 190,
    height: 40,
    borderRadius: 4,
    backgroundColor: '#ffffff'
  },
  normalText,
  mtfTabTextStyle,
  props => ({
    border: `1px solid ${props.color.grey1}`,
    color: props.color.black1,
    maxWidth: 'calc(50% - 25px)',
    padding: props.isMobile ? '7px 10px' : '10px',
    height: 'unset',
    textAlign: 'center'
  })
);

const RightSectionBox = styled.span({
  display: 'flex',
  maxWidth: 'calc(50% - 25px)'
});

const SingleMatchTheFollowingTab = styled.div({
  display: 'flex',
  alignItems: 'center',
  margin: '20px 0px'
});

const MatchTheFollowingDiv = styled.div`
  ${({ isMobile }) => `
    padding: ${isMobile ? '0 20px' : '0 0 0 50px'};
  `}
`;

const borderColorObj = {
  correct: 'green',
  wrong: 'red',
  selected: 'accent',
  default: 'grey1'
};

const getSelectStyle = (color, isMobile) => {
  const classObj = `
    .Select {
      display: inline-block;
      margin: 0;
      width: ${isMobile ? 'calc(50vw - 30px)' : '240px'};
      height: unset;
      border-radius: 4px;
      border: 1px solid ${color[borderColorObj.default]};
    }
    .Select > div {
      border: none;
    }
    .Select div[role="button"]::after,
    .Select div[role="button"]::before {
      transition: none;
    }
    .Select div[role="button"]:hover::before {
      border-bottom: none !important;
    }
    .Select div[role="button"]:focus::after,
    .Select div[role="button"]:active::after {
      border-bottom: none !important;
    }
    .Select div[role="button"] {
      min-height: 38px;
      border: none;
      color: ${color[borderColorObj.default]};
      padding: 10px 15px;
      border-radius: 10px;
    }
    .correct .Select div[role="button"] {
      color: ${color[borderColorObj.correct]};
    }
    .wrong .Select div[role="button"] {
      color: ${color[borderColorObj.wrong]};
    }
    .selected .Select div[role="button"] {
      color: ${color[borderColorObj.selected]};
    }
    .Select .Icon {
      color: ${color[borderColorObj.default]};
      fill: ${color[borderColorObj.default]};
    }
    .wrong .Select .Icon {
      color: ${color[borderColorObj.wrong]};
      fill: ${color[borderColorObj.wrong]};
    }
    .correct .Select .Icon {
      color: ${color[borderColorObj.correct]};
      fill: ${color[borderColorObj.correct]};
    }
    .selected .Select .Icon {
      color: ${color[borderColorObj.selected]};
      fill: ${color[borderColorObj.selected]};
    }
    .correct .Select {
      border: 1px solid ${color[borderColorObj.correct]};
    }
    .wrong .Select {
      border: 1px solid ${color[borderColorObj.wrong]};
    }
    .selected .Select {
      border: 1px solid ${color[borderColorObj.selected]};
    }
    .clear-text-drop-down {
      color: ${color.red};
      padding-top: 2px;
      padding-bottom: 2px;
      display: flex;
    }
    .Select button:last-child {
      margin-bottom: -8px;
      border-top: 1px solid ${color.grey1};
    }
  `;
  return classObj;
};

const matchingDotsColor = {
  default: {
    left: 'accent',
    right: 'grey1'
  },
  correct: {
    left: 'green',
    right: 'green'
  },
  wrong: {
    left: 'red',
    right: 'red'
  },
  selected: {
    left: 'accent',
    right: 'accent'
  }
};

const getMatchingDotsStyle = (color, isMobile) => {
  const matchingDotsStyle = `
    .matching-dots {
      width: ${isMobile ? '40px' : '71px'};
      height: 1px;
      position: relative;
      border: 0.5px solid ${color[matchingDotsColor.default.right]};
    }
    .matching-dots.correct {
      border: 0.5px solid ${color[matchingDotsColor.correct.right]};
    }
    .matching-dots.wrong {
      border: 0.5px solid ${color[matchingDotsColor.wrong.right]};
    }
    .matching-dots.selected {
      border: 0.5px solid ${color[matchingDotsColor.selected.right]};
    }
    .matching-dots-before {
      height: 10px;
      width: 10px;
      border-radius: 50%;
      display: flex;
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      z-index: 2;
      left: -6px;
      background-color: ${color[matchingDotsColor.default.left]};
    }
    .matching-dots-after {
      height: 10px;
      width: 10px;
      border-radius: 50%;
      display: flex;
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      z-index: 2;
      right: -5px;
      background-color: ${color[matchingDotsColor.default.right]};
    }
    .correct .matching-dots-before,
    .correct .matching-dots-after {
      background-color: ${color[matchingDotsColor.correct.left]};
    }
    .wrong .matching-dots-before,
    .wrong .matching-dots-after {
      background-color: ${color[matchingDotsColor.wrong.left]};
    }
    .selected .matching-dots-before,
    .selected .matching-dots-after {
      background-color: ${color[matchingDotsColor.selected.left]}
    }
  `;
  return matchingDotsStyle;
};

export {
  LightTextComponent,
  NormalTextComponent,
  MatchTheFollowingDiv,
  getSelectStyle,
  getMatchingDotsStyle,
  LeftSectionBox,
  RightSectionBox,
  SingleMatchTheFollowingTab,
  MatchTheFollowingTabsText
};
