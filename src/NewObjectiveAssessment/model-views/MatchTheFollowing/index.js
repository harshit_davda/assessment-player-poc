import React from "react";
import { observer } from "mobx-react";
import { Select } from "@lyearn-team/frontend-components";
import cloneDeep from "lodash/cloneDeep";
import difference from "lodash/difference";
import { knuthShuffle } from "knuth-shuffle";
import {
  LightTextComponent,
  NormalTextComponent,
  MatchTheFollowingDiv,
  getSelectStyle,
  getMatchingDotsStyle,
  LeftSectionBox,
  RightSectionBox,
  SingleMatchTheFollowingTab,
  MatchTheFollowingTabsText
} from "./style";
import {
  checkQuestionCompleted,
  getMTFLeftRightOptions,
  getMTFSelectedLeftOptions
} from "../../helpers";

@observer
class MatchTheFollowing extends React.Component {
  constructor(props) {
    super(props);
    const { currentQuestionState } = props.model.store;
    this.rightOptionsShuffled = false;
    this.shuffedRightOptionsArr = [];
    this.shuffledRightOptions = [];
    this.state = {
      selectedLeftOptions: getMTFSelectedLeftOptions(currentQuestionState)
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { currentQuestionState } = nextProps.model.store;
    const selectedLeftOptions = getMTFSelectedLeftOptions(currentQuestionState);
    this.setState({ selectedLeftOptions });
  }

  selected = selectedObj => {
    const { testFinished } = this.props.model.store;
    if (!testFinished) {
      const { selectedLeftOptions } = this.state;
      if (selectedObj._id === "clearSelection_id") {
        selectedLeftOptions[selectedObj.leftId] = "";
      } else {
        Object.keys(selectedLeftOptions).forEach(key => {
          if (selectedLeftOptions[key] === selectedObj.rightId) {
            selectedLeftOptions[key] = "";
          }
        });
        selectedLeftOptions[selectedObj.leftId] = selectedObj.rightId;
      }
      this.setState(
        {
          selectedLeftOptions
        },
        () => {
          this.props.model.actions.updateMTFOptions(this.getResponseString());
        }
      );
    }
  };

  getResponseString = () => {
    const payload = [];
    Object.keys(this.state.selectedLeftOptions).forEach(key => {
      payload.push({
        leftId: key,
        rightId: this.state.selectedLeftOptions[key] || ""
      });
    });
    return JSON.stringify(payload);
  };

  selectCustomOptions = ({ option: { value } }) => (
    <MatchTheFollowingTabsText
      dangerouslySetInnerHTML={{
        __html: value
      }}
    />
  );

  getMatchingDots = (selectionStatus, color, isMobile) => {
    const matchingDotsDiv = (
      <span>
        <style>{getMatchingDotsStyle(color, isMobile)}</style>
        <div className={`matching-dots ${selectionStatus}`}>
          <div className="matching-dots-before" />
          <div className="matching-dots-after" />
        </div>
      </span>
    );
    return matchingDotsDiv;
  };

  getCorrectAnsObj = pairs => {
    const correctAnsObj = {};
    pairs.forEach(item => {
      correctAnsObj[item.left._id] = item.right._id;
    });
    return correctAnsObj;
  };

  getMatchTheFollowingHeader = ({
    color,
    isMobile,
    questionText,
    maxScore,
    isGradable
  }) => {
    const header = [];
    header.push(
      <NormalTextComponent
        color={color}
        isMobile={isMobile}
        dangerouslySetInnerHTML={{
          __html: questionText
        }}
      />
    );
    return header;
  };

  getMatchTheFollowingBody = ({
    checkAnswer,
    seeCorrectAnswers,
    updateAnswerStatus,
    testFinished,
    revealOnTestFinish,
    leftOptions,
    rightOptions,
    correctAnsObj,
    color,
    isMobile
  }) => {
    const body = [];
    leftOptions.forEach(item => {
      let selectionStatus = "default";
      let selectOptions = cloneDeep(rightOptions);
      const selectedValues = [];
      let selectedLeftOptions = this.state.selectedLeftOptions;
      selectedLeftOptions = seeCorrectAnswers
        ? correctAnsObj
        : selectedLeftOptions;
      const selectedRightId = selectedLeftOptions[item._id];
      selectOptions.forEach(rightItem => {
        rightItem.leftId = item._id;
        rightItem.rightId = rightItem._id;
        if (selectedRightId === rightItem.rightId) {
          selectedValues.push(rightItem);
          selectionStatus = "selected";
          if (checkAnswer || revealOnTestFinish) {
            if (correctAnsObj[item._id] === rightItem._id) {
              selectionStatus = "correct";
            } else {
              selectionStatus = "wrong";
            }
            selectionStatus === "wrong" && updateAnswerStatus("wrong");
          }
          selectionStatus = seeCorrectAnswers ? "correct" : selectionStatus;
        }
      });
      selectOptions = selectOptions.filter(
        itm =>
          Object.values(this.state.selectedLeftOptions).indexOf(itm._id) === -1
      );
      const clearBtnProps = {
        _id: "clearSelection_id",
        text: "",
        label: "",
        leftId: item._id,
        rightId: ""
      };
      body.push(
        <SingleMatchTheFollowingTab>
          <LeftSectionBox
            color={color}
            isMobile={isMobile}
            dangerouslySetInnerHTML={{
              __html: item.text
            }}
          />
          {this.getMatchingDots(selectionStatus, color, isMobile)}
          <RightSectionBox className={`${selectionStatus}`}>
            <style>{getSelectStyle(color, isMobile)}</style>
            <Select
              onSelect={this.selected}
              label="Select"
              options={selectOptions}
              minWidth={100}
              selectedOptions={selectedValues}
              customOption={this.selectCustomOptions}
              disabled={testFinished}
              showClearBtn={true}
              clearBtnProps={clearBtnProps}
            />
          </RightSectionBox>
        </SingleMatchTheFollowingTab>
      );
    });
    return body;
  };

  getShuffledRightOptions = rightOptions => {
    if (this.rightOptionsShuffled) {
      const rightOptionsArr = [];
      rightOptions.forEach(item => {
        rightOptionsArr.push(item._id);
      });
      const isDropDownChanged = difference(
        rightOptionsArr,
        this.shuffedRightOptionsArr
      );
      return isDropDownChanged.length
        ? knuthShuffle(rightOptions)
        : this.shuffledRightOptions;
    }
    const shuffledRightOpt = knuthShuffle(rightOptions);
    shuffledRightOpt.forEach(item => {
      this.shuffedRightOptionsArr.push(item._id);
    });
    this.shuffledRightOptions = shuffledRightOpt;
    this.rightOptionsShuffled = true;
    return shuffledRightOpt;
  };

  render() {
    const {
      isMobile,
      checkAnswer,
      seeCorrectAnswers,
      testFinished,
      revealOnTestFinish,
      theme,
      currentQuestion,
      currentQuestionState,
      isGradable
    } = this.props.model.store;
    const { maxScore, content } = currentQuestion;
    const { questionText, pairs } = content.question;
    const correctAnsObj = this.getCorrectAnsObj(pairs);
    const { color } = theme;
    const { leftOptions, rightOptions } = getMTFLeftRightOptions(pairs);
    const shuffledRightOptions = this.getShuffledRightOptions(rightOptions);
    const {
      updateAnswerStatus,
      disableCheckBtnStatus
    } = this.props.model.actions;
    const questionComplete = checkQuestionCompleted({
      currentQuestion,
      currentQuestionState
    });
    disableCheckBtnStatus(!questionComplete);
    !questionComplete &&
      (checkAnswer || revealOnTestFinish) &&
      updateAnswerStatus("wrong");
    return (
      <MatchTheFollowingDiv isMobile={isMobile}>
        {this.getMatchTheFollowingHeader({
          color,
          isMobile,
          questionText,
          maxScore,
          isGradable
        })}
        {this.getMatchTheFollowingBody({
          checkAnswer,
          seeCorrectAnswers,
          updateAnswerStatus,
          testFinished,
          revealOnTestFinish,
          leftOptions,
          rightOptions: shuffledRightOptions,
          correctAnsObj,
          color,
          isMobile
        })}
      </MatchTheFollowingDiv>
    );
  }
}

export default MatchTheFollowing;
