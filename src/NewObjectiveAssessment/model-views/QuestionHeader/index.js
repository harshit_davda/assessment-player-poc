import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Icon } from '@lyearn-team/frontend-components';
import styled from 'react-emotion';
import { css } from 'emotion';
import moment from 'moment';
import 'moment-duration-format';
import GridIcon from '../../components/GridIcon';
import AssessmentOptionsIcon from '../../components/AssessmentOptionsIcon';
import QuestionCount from '../../components/QuestionCount';
import ScoreCardHeaderFeedback from '../../components/TestFinishedHeaderFeedback';
import ReviewQuestionsText from '../../components/ReviewQuestionsText';

const QuestionHeaderContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  ${({ isMobile }) => `
    padding: ${isMobile ? '20px' : '20px 50px'};
    min-height: ${isMobile ? '75px' : '88px'};
  `}
  @media (min-width: 767px) and (max-height: 700px){
    padding: 5px 50px;
    min-height: 50px;
  }
`;

const timerWrapper = css`
  display: flex;
  align-items: center;
  font-family: ProximaNova;
`;

const seprator = theme => css`
  width: 1px;
  border: 0.5px solid ${theme.color.grey1};
  margin-right: 15px;
  margin-left: 15px;
  height: 16px;
`;

const timeStringClass = theme => css`
  letter-spacing: 0.8px;
  font-size: 16px;
  color: ${theme.color.black2};
  font-family: ProximaNova;
`;

const clockboxClass = (theme, isWarningState) => css`
  display: flex;
  border-radius: 4px;
  align-items: center;
  padding: 8px;
  margin-right: -8px;

  ${isWarningState
    ? `
    color: ${theme.color.red};
    background: ${theme.color.redHighlight};
    border: 1px solid ${theme.color.red};
    p {
      color: ${theme.color.red};
    }
    svg {
      fill: ${theme.color.red} !important;
    }
  `
    : ''}
`;

@observer
class QuestionHeader extends Component {
  componentDidMount() {
    const { timeLeft, card, testFinished } = this.props.model.store;
    if (timeLeft < 0 && !testFinished) {
      this.props.model.actions.finishTest();
    }
    if (card.timeBoundTest && !testFinished) {
      this.timeoutRef = setInterval(() => {
        const { timeLeft: left } = this.props.model.store;
        if (left <= 0 && left !== null) {
          clearInterval(this.timeoutRef);
        } else {
          this.props.model.actions.setTimeLeft();
          if (
            this.props.model.store.timeLeft <= 0 &&
            this.props.model.store.timeLeft !== null &&
            !testFinished
          ) {
            clearInterval(this.timeoutRef);
            this.props.model.actions.finishTest();
          }
        }
      }, 1000);
    }
  }

  componentDidUpdate(prevProps) {
    const { card, testFinished } = this.props.model.store;
    if (card._id !== prevProps.model.store.card._id) {
      clearInterval(this.timeoutRef);
      if (card.timeBoundTest && !testFinished) {
        this.timeoutRef = setInterval(() => {
          const { timeLeft: left } = this.props.model.store;
          if (left <= 0 && left !== null) {
            clearInterval(this.timeoutRef);
          } else {
            this.props.model.actions.setTimeLeft();
            if (
              this.props.model.store.timeLeft <= 0 &&
              this.props.model.store.timeLeft !== null &&
              !testFinished
            ) {
              clearInterval(this.timeoutRef);
              this.props.model.actions.finishTest();
            }
          }
        }, 1000);
      }
    }
  }

  componentWillUnmount() {
    clearInterval(this.timeoutRef);
  }

  renderLeftInfo = () => {
    const { store } = this.props.model;
    const { theme, currentQuestionIndex, questionsLength, gridView } = store;
    if (gridView) {
      return <ReviewQuestionsText theme={theme} />;
    }
    return (
      <QuestionCount
        theme={theme}
        currentQuestionIndex={currentQuestionIndex}
        questionsLength={questionsLength}
      />
    );
  };

  render() {
    const { store, actions } = this.props.model;
    const {
      theme,
      gridView,
      assessmentOptionsOpen,
      showScoreCard,
      isMobile,
      cardState,
      card,
      isGradable,
      testFinished
    } = store;
    const { toggleGridView, toggleAssessmentOptions } = actions;
    const localToggleGridView = from => () => toggleGridView(from);

    if (showScoreCard) {
      const { successStatus } = cardState;
      let passed = successStatus === 'pass';
      if (!isGradable) {
        const { children } = card;
        let totalMinScore = 0;
        let totalRawScore = 0;
        children.forEach(item => {
          if (item.state.actualSubmit) {
            totalRawScore += item.state.rawScore;
            totalMinScore = item.cardQuestion.minScore;
          }
        });
        passed = totalRawScore > totalMinScore ? 'pass' : 'fail';
      }
      return (
        <QuestionHeaderContainer isMobile={isMobile}>
          <ScoreCardHeaderFeedback
            theme={theme}
            passed={passed}
            isMobile={isMobile}
            isGradable={isGradable}
          />
        </QuestionHeaderContainer>
      );
    }
    const { timeLeft } = store;
    const showTimer = card.timeBoundTest && timeLeft > 0 && !testFinished;
    const duration = moment.duration(timeLeft / 1000, 'seconds');
    let timeInString = duration.format('hh:mm:ss');
    if (timeInString.length === 2) {
      timeInString = `00:${timeInString}`;
    }
    const isWarningState = card.sixtySecWarning && timeLeft / 1000 <= 60;
    return (
      <QuestionHeaderContainer isMobile={isMobile}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <GridIcon theme={theme} selected={gridView} onGridClick={localToggleGridView('header')} />
          {this.renderLeftInfo()}
        </div>
        <div className={timerWrapper}>
          {showTimer && (
            <React.Fragment>
              <div className={clockboxClass(theme, isWarningState)}>
                <Icon
                  type="clock"
                  style={{
                    fill: theme.color.black3,
                    marginRight: '8px',
                    height: '18px',
                    width: '18px'
                  }}
                />
                <p className={timeStringClass(theme)}>{timeInString}</p>
              </div>
              <div className={seprator(theme)} />
            </React.Fragment>
          )}
          <AssessmentOptionsIcon
            theme={theme}
            isOpen={assessmentOptionsOpen}
            onClick={toggleAssessmentOptions}
          />
        </div>
      </QuestionHeaderContainer>
    );
  }
}

export default QuestionHeader;
