import React, { useCallback } from 'react';
import { css } from 'emotion';
import { AccentButton } from '@lyearn-team/frontend-components';

const headerWrapper = theme => css`
  width: 100%;
  background-color: #fff;
  padding: 13px;
  position: fixed;
  top: 0;
  border-bottom: 1px solid ${theme.color.grey3};
`;

const headerContentWrapper = css`
  display: flex;
  max-width: 800px;
  justify-content: space-between;
  margin: 0 auto;
  align-items: center;
`;

const headerTextClass = theme => css`
  font-size: 32px;
  color: ${theme.color.black1};
  font-family: Proximanova;
`;

function Header(props) {
  const { model } = props;
  const { cardListMenuBtn, updateAssignmentScreenStatus, card, theme } = model.store;

  const closeScreen = useCallback(() => {
    updateAssignmentScreenStatus(false);
    cardListMenuBtn();
  }, [cardListMenuBtn, updateAssignmentScreenStatus]);

  return (
    <div className={headerWrapper(theme)}>
      <div className={headerContentWrapper}>
        <p className={headerTextClass(theme)}>{card.content.name}</p>
        <AccentButton
          onClick={closeScreen}
          text="Exit Full Screen"
          icon={{
            type: 'backToNormal',
            style: { width: '18px', marginRight: '6px' }
          }}
          style={{ margin: '10px', color: theme.color.black3 }}
        />
      </div>
    </div>
  );
}

export default Header;
