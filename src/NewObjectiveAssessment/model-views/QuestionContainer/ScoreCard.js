import React, { Component } from "react";
import { observer } from "mobx-react";
import styled from "react-emotion";
import { marksUptoTwoDecimal } from "../../helpers";

const ScoreCardWrapper = styled.div`
  width: 100%;
  font-family: ProximaNova;
  font-size: 24px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1;
  flex-grow: 1;
  letter-spacing: normal;
  overflow: auto;
  ${props => `
    color: ${props.theme.color.black3};
  `};
  ${({ isMobile }) => `
    padding: ${isMobile ? "40px 15px" : "32.5px 175px"};
  `}
`;

const ScoreWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  height: 66px;
  padding: 21px 36px;
  font-family: ProximaNova;
  font-size: 34px;
  font-weight: 600;
  font-style: normal;
  font-stretch: normal;
  line-height: 0.71;
  letter-spacing: normal;
  margin-bottom: 42px;
  border-radius: 6px;
  ${props =>
    props.isGradable
      ? props.passed
        ? `
  background-color: #f5fff6;
  color: #39b54a;
`
        : `
  background-color: #fff5f5;
  color: #f85359;
`
      : `
    background-color: #f2f6fc;
    color: #707a85;
  `}
`;

const InfoWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 42px;
  padding: 0 40px;
`;

const getDate = t => {
  let date;
  try {
    date = new Date(t);
  } catch (e) {
    return new Date();
  }
  return date;
};

const doubleDigit = d => (d ? (d.length === 1 ? "0" + d : d) : "00");

const timeDifference = (d1, d2) => {
  const date1 = getDate(d1);
  const date2 = getDate(d2);
  let difference = date1.getTime() - date2.getTime();
  const daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
  difference -= daysDifference * 1000 * 60 * 60 * 24;
  const hoursDifference = Math.floor(difference / 1000 / 60 / 60);
  difference -= hoursDifference * 1000 * 60 * 60;
  const minutesDifference = Math.floor(difference / 1000 / 60);
  difference -= minutesDifference * 1000 * 60;
  const secondsDifference = Math.floor(difference / 1000);
  return (
    doubleDigit(hoursDifference) +
    ":" +
    doubleDigit(minutesDifference) +
    ":" +
    doubleDigit(secondsDifference)
  );
};

@observer
class ScoreCard extends Component {
  render() {
    const {
      theme,
      card,
      cardState,
      isMobile,
      penaltyMarks = 0,
      isGradable
    } = this.props.model.store;
    const { maxScore = 0, minScaledScore = 0 } = card;
    const { children } = card;
    let totalRawScore = 0;
    let totalMaxScore = 0;
    let totalMinScore = 0;
    const {
      rawScore,
      startTimestamp,
      lastSubmittedOn,
      successStatus
    } = cardState;
    const timeTaken =
      startTimestamp && timeDifference(lastSubmittedOn, startTimestamp);
    let passed = successStatus === "pass";
    let totalPenalty = 0;
    if (!isGradable) {
      children.forEach(item => {
        if (item.state.actualSubmit) {
          if (
            item.state.submissions &&
            item.state.submissions[0].answerEvaluation.toUpperCase() === "TRUE"
          ) {
            totalRawScore += 1;
          }
          totalMaxScore += 1;
          totalMinScore += 0;
        }
      });
      passed = totalRawScore > totalMinScore ? "pass" : "fail";
    } else {
      children.forEach(item => {
        const answerEvaluation = item.state.submissions[0].answerEvaluation;
        if (answerEvaluation && answerEvaluation.toUpperCase() === "FALSE") {
          totalPenalty += penaltyMarks;
        }
      });
    }
    const yourScore = !isGradable ? totalRawScore : rawScore;
    return (
      <ScoreCardWrapper theme={theme} isMobile={isMobile}>
        <ScoreWrapper passed={passed} isGradable={isGradable}>
          <div>Your Score</div>
          <div>{yourScore}</div>
        </ScoreWrapper>
        {isGradable ? (
          <React.Fragment>
            <InfoWrapper>
              <div>Maximum Score</div>
              <div>{maxScore}</div>
            </InfoWrapper>
            <InfoWrapper>
              <div>Passing Score</div>
              <div>{marksUptoTwoDecimal(maxScore * minScaledScore)}</div>
            </InfoWrapper>
            {totalPenalty ? (
              <InfoWrapper>
                <div>Penalty Score</div>
                <div>{marksUptoTwoDecimal(totalPenalty)}</div>
              </InfoWrapper>
            ) : null}
          </React.Fragment>
        ) : (
          <InfoWrapper>
            <div>Total Score</div>
            <div>{marksUptoTwoDecimal(totalMaxScore)}</div>
          </InfoWrapper>
        )}
      </ScoreCardWrapper>
    );
  }
}

export default ScoreCard;
