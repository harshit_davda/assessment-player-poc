import React, { Component } from 'react';
import { observer } from 'mobx-react';
import styled from 'react-emotion';
import NewSelect from '../NewSelect';
import FillInTheBlanks from '../FillInTheBlanks';
import MatchTheFollowing from '../MatchTheFollowing';
import FeedBackComponent from '../../components/FeedBackComponent';
import GridView from '../GridView';
import AssessmentOptionsOverlay from '../AssessmentOptionsOverlay';
import ScoreCard from './ScoreCard';
import FooterFeedback from '../../components/FooterFeedback';
import { getObjectiveResponseType } from '../../helpers';

const QuestionContentContainer = styled.div`
  position: relative;
  width: 100%;
  flex-grow: 1;
  overflow-y: auto;
  overflow-x: hidden;
  ${({ isMobile }) => `
    padding: ${isMobile ? '20px 0' : '40px 0'};
  `}
  @media (min-width: 767px) and (max-height: 700px){
    padding: 5px 20px;
    min-height: 50px;
  }
`;

@observer
class QuestionContainer extends Component {
  renderQuestionContentSwitch = () => {
    const { model } = this.props;
    const { currentQuestion } = model.store;
    const questionId = currentQuestion._id;
    const responseType = getObjectiveResponseType(currentQuestion);
    switch (responseType) {
      case 'MULTIPLE_SELECT':
      case 'SINGLE_SELECT': {
        return <NewSelect model={model} key={questionId} />;
      }
      case 'FILL_IN_THE_BLANKS':
        return <FillInTheBlanks model={model} key={questionId} />;
      case 'MATCH_THE_FOLLOWING':
        return <MatchTheFollowing model={model} key={questionId} />;
      default:
        return null;
    }
  };

  render() {
    const { model } = this.props;
    const {
      gridView,
      showScoreCard,
      isMobile,
      answerStatus,
      card,
      currentQuestionIndex
    } = model.store;
    const currentCardQuestion = card.children[currentQuestionIndex];
    const correctFeedback = currentCardQuestion.cardQuestion.content.question.correctAnswerFeedback;
    const wrongFeedback = currentCardQuestion.cardQuestion.content.question.incorrectAnswerFeedback;
    const showFeedBack = answerStatus
      && ((answerStatus === 'correct' && correctFeedback)
        || (answerStatus === 'wrong' && wrongFeedback));
    const feedBackProps = {
      heading: 'FEEDBACK',
      body:
        (answerStatus === 'correct' && correctFeedback)
        || (answerStatus === 'wrong' && wrongFeedback),
      color: this.props.model.store.theme.color,
      isMobile
    };
    if (showScoreCard) {
      return <ScoreCard model={model} />;
    }
    return (
      <QuestionContentContainer isMobile={isMobile}>
        {gridView ? <GridView model={model} /> : this.renderQuestionContentSwitch()}
        <AssessmentOptionsOverlay model={model} />
        {showFeedBack ? <FeedBackComponent {...feedBackProps} /> : null}
        {isMobile && answerStatus ? (
          <FooterFeedback isMobile={isMobile} isCorrect={answerStatus === 'correct'} />
        ) : null}
      </QuestionContentContainer>
    );
  }
}

export default QuestionContainer;
