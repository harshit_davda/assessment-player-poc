import React, { Component } from "react";
import { Icon } from "@lyearn-team/frontend-components";
import { observer } from "mobx-react";
import styled from "react-emotion";

const OverlayWrapper = styled.div`
  position: absolute;
  width: 259px;
  height: 100%;
  left: 101%;
  top: 0;
  box-shadow: -2px 0 4px 0 rgba(0, 0, 0, 0.12);
  background-color: #f5f9ff;
  z-index: 100;
  transition: transform;
  transition-timing-function: ease-in-out;
  transition-duration: 0.6s;
  ${props =>
    props.isOpen
      ? `
    transform: translateX(-266px);
  `
      : `
    transform: translateX(0px);
  `};
`;

const Option = styled.div`
  width: 100%;
  cursor: pointer;
  font-family: ProximaNova;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  ${props => `
    color: ${props.theme.color.black3};
  `};
  ${({ disabled }) => `
    ${disabled &&
      `
      cursor: no-drop;
      opacity: 0.5;
    `}
  `};
  padding: 15px 26px;
  border-bottom: 2px solid #e7ecf2;
`;

@observer
class AssessmentOptionsOverlay extends Component {
  updateAssignmentScreenStatus = () => {
    if (
      !this.props.model.store.isAssignmentFullScreen &&
      this.props.model.store.menuOpen
    ) {
      const {
        updateAssignmentScreenStatus,
        cardListMenuBtn
      } = this.props.model.store;
      this.props.model.actions.toggleAssessmentOptions();
      updateAssignmentScreenStatus(true);
      cardListMenuBtn();
    }
  };

  closeFullScreen = () => {
    if (this.props.model.store.isAssignmentFullScreen) {
      const {
        updateAssignmentScreenStatus,
        cardListMenuBtn
      } = this.props.model.store;
      this.props.model.actions.toggleAssessmentOptions();
      updateAssignmentScreenStatus(false);
      cardListMenuBtn();
    }
  };

  render() {
    const { model, windowWidth } = this.props;
    const { store, actions } = model;
    const {
      assessmentOptionsOpen,
      theme,
      isAssignmentFullScreen,
      testFinished
    } = store;
    const { flagQuestion } = actions;
    const isMobile = windowWidth < 760;
    return (
      <OverlayWrapper isOpen={assessmentOptionsOpen}>
        <Option theme={theme} onClick={flagQuestion} disabled={testFinished}>
          <Icon type="flag" style={{ marginRight: "17px" }} />
          Flag this Question
        </Option>
        {/* <Option
          theme={theme}
          onClick={
            isAssignmentFullScreen
              ? this.closeFullScreen
              : this.updateAssignmentScreenStatus
          }
          disabled={isMobile}
        >
          <Icon
            type={isAssignmentFullScreen ? "backToNormal" : "fullScreen"}
            style={{ marginRight: "17px", fill: theme.color.black3 }}
          />
          {isAssignmentFullScreen ? (
            <span>Exit Full Screen</span>
          ) : (
            <span>Full Screen</span>
          )}
        </Option> */}
      </OverlayWrapper>
    );
  }
}

export default AssessmentOptionsOverlay;
