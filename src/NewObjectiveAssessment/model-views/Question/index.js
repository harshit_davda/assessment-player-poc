import React, { Component } from "react";
import { observer } from "mobx-react";
import styled from "react-emotion";
import AssessmentStartScreen from "../AssessmentStartScreen";
import QuestionHeader from "../QuestionHeader";
import QuestionFooter from "../QuestionFooter";
import QuestionContentContainer from "../QuestionContainer";
import ProgressBar from "../../components/ProgressBar";

export const QuestionContainer = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #ffffff;
  border: 1px solid #e7ecf2;
  width: 100%;
  position: relative;
  overflow: hidden;
  box-sizing: border-box;
  ${({ isAssignmentFullScreen }) => `
    ${
      !isAssignmentFullScreen
        ? `
      
    `
        : `
      height: calc(100vh - 200px);
      max-width: 800px;
      margin-top: 50px;
    `
    }
  `}
  ${({ isMobile }) => `
    border-radius: ${isMobile ? "0px" : "15px"};
    border-top: 1px solid #e7ecf2;
    border-bottom: 1px solid #e7ecf2;
    ${isMobile ? "height: calc(100vh)" : "height: calc(100vh - 170px)"};
  `}
  @media (min-width: 767px) and (max-height: 700px) {
    height: calc(100vh - 210px);
  }
`;

@observer
class Question extends Component {
  render() {
    const { model } = this.props;
    const {
      progressPercent,
      showTestStartScreen,
      isMobile,
      isAssignmentFullScreen
    } = model.store;
    if (showTestStartScreen) {
      return (
        <QuestionContainer
          isAssignmentFullScreen={isAssignmentFullScreen}
          isMobile={isMobile}
        >
          <AssessmentStartScreen model={model} />
        </QuestionContainer>
      );
    }
    return (
      <QuestionContainer
        isMobile={isMobile}
        isAssignmentFullScreen={isAssignmentFullScreen}
      >
        <QuestionHeader model={model} />
        <ProgressBar progressPercent={progressPercent} />
        <QuestionContentContainer model={model} />
        <QuestionFooter model={model} />
      </QuestionContainer>
    );
  }
}

export default Question;
