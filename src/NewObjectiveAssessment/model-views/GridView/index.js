import React, { Component } from 'react';
import { observer } from 'mobx-react';
import styled from 'react-emotion';
import GridElement from './GridElement';
import GridViewControls from './GridViewControls';
import { checkQuestionCompleted } from '../../helpers';

const GridViewContainer = styled.div`
  border-radius: 15px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.12);
  background-color: #f5f9ff;
  ${({ isMobile }) => `
    margin: ${isMobile ? '20px' : '0px 50px'};
    padding: ${isMobile ? '15px' : '30px 35px 60px'};
  `}
`;

const GridElementsWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  ${({ isMobile }) => `
    margin: ${isMobile ? '-7.5px' : '-6px -8px'};
  `}
`;

@observer
class GridView extends Component {
  constructor(props) {
    super(props);
    this.questionsLength = props.model.store.questionsLength;
    this.maxQuestionToShow = 24;
    this.state = {
      minQuestionIndex: 0,
      maxQuestionIndex:
        this.questionsLength > this.maxQuestionToShow
          ? this.maxQuestionToShow
          : this.questionsLength
    };
  }

  onNavigationClicked = type => () => {
    const { minQuestionIndex, maxQuestionIndex } = this.state;
    if (type === 'NEXT') {
      if (this.questionsLength > maxQuestionIndex) {
        const difference = this.questionsLength - maxQuestionIndex;
        if (difference > this.maxQuestionToShow) {
          this.setState({
            maxQuestionIndex: maxQuestionIndex + this.maxQuestionToShow,
            minQuestionIndex: minQuestionIndex + this.maxQuestionToShow
          });
        } else {
          this.setState({
            maxQuestionIndex: maxQuestionIndex + difference,
            minQuestionIndex: minQuestionIndex + this.maxQuestionToShow
          });
        }
      }
    } else if (type === 'PREV') {
      if (minQuestionIndex > 0) {
        const difference = maxQuestionIndex - minQuestionIndex;
        if (difference === this.maxQuestionToShow) {
          this.setState({
            maxQuestionIndex: maxQuestionIndex - this.maxQuestionToShow,
            minQuestionIndex: minQuestionIndex - this.maxQuestionToShow
          });
        } else {
          this.setState({
            maxQuestionIndex: maxQuestionIndex - difference,
            minQuestionIndex: minQuestionIndex - this.maxQuestionToShow
          });
        }
      }
    }
  };

  goToQuestion = index => () => {
    this.props.model.actions.goToQuestion(index);
  };

  render() {
    const { maxQuestionIndex, minQuestionIndex } = this.state;
    const { store } = this.props.model;
    const {
      theme,
      questionsLength,
      card,
      currentQuestionIndex,
      isMobile,
      revealCorrectAnsGrid
    } = store;
    const gridElementList = [];
    for (let index = minQuestionIndex; index < maxQuestionIndex; index += 1) {
      const element = card.children[index];
      const submissions = element.state.submissions[0];
      const isAnswered = checkQuestionCompleted({
        currentQuestion: element.cardQuestion,
        currentQuestionState: element.state
      });
      const answerEvaluation = submissions.answerEvaluation || '';
      const selectionStatus = answerEvaluation
        ? answerEvaluation.toLowerCase() === 'true'
          ? 'correct'
          : 'wrong'
        : 'default';
      gridElementList.push(
        <GridElement
          key={element.cardQuestion._id}
          theme={theme}
          isCurrent={index === currentQuestionIndex}
          index={index}
          isFlagged={element.state.flagged}
          isAnswered={isAnswered}
          onGridElementClick={this.goToQuestion}
          revealCorrectAnsGrid={revealCorrectAnsGrid}
          selectionStatus={selectionStatus}
          isMobile={isMobile}
        />
      );
    }
    return (
      <GridViewContainer isMobile={isMobile}>
        <GridViewControls
          theme={theme}
          maxQuestionIndex={maxQuestionIndex}
          questionsLength={questionsLength}
          minQuestionIndex={minQuestionIndex}
          onNavigationClicked={this.onNavigationClicked}
          maxQuestionToShow={this.maxQuestionToShow}
        />
        <GridElementsWrapper isMobile={isMobile}>{gridElementList}</GridElementsWrapper>
      </GridViewContainer>
    );
  }
}

export default GridView;
