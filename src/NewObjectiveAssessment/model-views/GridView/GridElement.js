import React from 'react';
import { Icon } from '@lyearn-team/frontend-components';
import styled from 'react-emotion';

const colorObj = {
  correct: {
    color: 'green',
    bg: '#f5fff6'
  },
  default: {
    color: 'black3',
    bg: '#fff'
  },
  wrong: {
    color: 'red',
    bg: '#fff5f5'
  },
  selected: {
    color: 'accent',
    bg: '#fff'
  }
};

const GridElementContainer = styled.div`
  position: relative;
  width: 55px;
  height: 55px;
  border-radius: 4px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #ffffff;
  cursor: pointer;
  ${({ isMobile }) => `
    margin: ${isMobile ? '7.5px' : '10px 10.2px'};
  `}
  ${props => `
    border: 1px solid;
    border-color: ${props.theme.color.black3};
  `}
  ${props => props.isAnswered
    && `
    border: 1px solid;
    border-bottom: 4px solid;
    border-color: ${props.theme.color.accent};
  `}
  ${props => props.isCurrent
    && `
    border: 2px solid;
    border-color: ${props.theme.color.accent};
  `}
  ${props => props.status
    && `
    border-color: ${props.theme.color[colorObj[props.status].color]};
    background-color: ${[colorObj[props.status].bg]};
  `}
  ${props => props.isCurrent
    && props.isAnswered
    && `
    border: 2px solid ${props.theme.color.accent};
    border-bottom: 4px solid ${props.theme.color.accent};
  `}
`;

const ArrowDown = styled.div`
  position: absolute;
  width: 0;
  height: 0;
  top: -1px;
  left: 50%;
  transform: translateX(-50%);
  border-left: 9px solid transparent;
  border-right: 9px solid transparent;
  ${props => `
    border-top: 11px solid ${props.theme.color[colorObj[props.status].color]};
    `};
`;

function GridElement({
  isCurrent,
  theme,
  isAnswered,
  onGridElementClick,
  index,
  isFlagged,
  revealCorrectAnsGrid,
  selectionStatus,
  isMobile
}) {
  const status = revealCorrectAnsGrid && selectionStatus;
  const propsToPass = {
    isCurrent,
    theme,
    isAnswered: !status ? isAnswered : null,
    onClick: onGridElementClick(index),
    revealCorrectAnsGrid,
    isMobile,
    status: (isAnswered && status) || null
  };
  return (
    <GridElementContainer {...propsToPass}>
      {isFlagged ? <Icon type="flagged" style={{ fill: '#929dae' }} /> : index + 1}
      {isCurrent && <ArrowDown theme={theme} status={(isAnswered && status) || 'selected'} />}
    </GridElementContainer>
  );
}

export default GridElement;
