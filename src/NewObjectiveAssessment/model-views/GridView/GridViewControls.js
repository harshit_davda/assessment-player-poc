import React from "react";
import { Icon } from "@lyearn-team/frontend-components";
import styled from "react-emotion";

const ArrowButton = styled.div`
  width: 30px;
  cursor: pointer;
  height: 30px;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  margin-right: 7px;
  align-items: center;
  ${props => `
    border: 1px solid ${props.theme.color.grey1};
  `};
`;

const Text = styled.div`
  margin-left: 5px;
  font-family: ProximaNova;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  ${props => `
    color: ${props.theme.color.black3};
  `};
`;

function GridViewControls({
  theme,
  minQuestionIndex,
  maxQuestionIndex,
  questionsLength,
  onNavigationClicked,
  maxQuestionToShow
}) {
  const showArrow = questionsLength > maxQuestionToShow;
  return (
    <div
      style={{ display: "flex", alignItems: "center", marginBottom: "18px" }}
    >
      {showArrow ? (
        <ArrowButton theme={theme} onClick={onNavigationClicked("PREV")}>
          <Icon type="angle-left" style={{ fill: "#929dae" }} />
        </ArrowButton>
      ) : null}
      {showArrow ? (
        <ArrowButton theme={theme} onClick={onNavigationClicked("NEXT")}>
          <Icon type="angle-right" style={{ fill: "#929dae" }} />
        </ArrowButton>
      ) : null}
      <Text theme={theme}>
        {minQuestionIndex + 1} - {maxQuestionIndex} of {questionsLength}
      </Text>
    </div>
  );
}

export default GridViewControls;
