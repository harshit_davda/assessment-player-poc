import React, { Component } from "react";
import { observer } from "mobx-react";
import { SquareBorderButton, Popup } from "@lyearn-team/frontend-components";
import styled from "react-emotion";
import { css } from "emotion";
import FooterFeedback from "../../components/FooterFeedback";

const QuestionFooterContainer = styled.div`
  min-height: 88px;
  width: 100%;
  border-top: 1px solid #e7ecf2;
  display: flex;
  justify-content: space-between;
  align-items: center;
  ${({ isMobile }) => `
    padding: ${isMobile ? "20px" : "20px 50px"};
  `}
  @media (min-width: 767px) and (max-height: 700px) {
    padding: 5px 50px;
    min-height: 50px;
  }

  button {
    padding: 0;
  }
`;

const QuestionFooterGridViewContainer = styled.div`
  min-height: 88px;
  width: 100%;
  border-top: 1px solid #e7ecf2;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  ${({ isMobile }) => `
    padding: ${isMobile ? "15px" : "20px 50px"};
  `}
`;

const unansweredTextStyle = css`
  margin-bottom: 20px;
`;

const ReattemptsCount = styled.div`
  font-family: ProximaNova;
  font-size: 14px;
  font-weight: normal;
  font-style: italic;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  ${({ theme }) => `
    color: ${theme.color.black2};
  `}
`;

const getGridIconStyle = () => {
  const styleStr = `
    .review-icon-grid #Group {
      color: #4a90e2;
      fill: #4a90e2;
    }
  `;
  return styleStr;
};

@observer
class QuestionFooter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      btnStatus:
        props.model.store.revealOnTestFinish === "DONT_REVEAL"
          ? ""
          : "checkAnswer"
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!nextProps.model.store.checkAnswer) {
      this.setState({
        btnStatus:
          nextProps.model.store.revealOnTestFinish === "DONT_REVEAL"
            ? ""
            : "checkAnswer"
      });
    }
  }

  nextQuestionClicked = () => {
    this.props.model.actions.goToNextQuestion();
  };

  previousQuestionClicked = () => {
    this.props.model.actions.goToPreviousQuestion();
  };

  onReviewClicked = from => () => {
    this.props.model.actions.toggleGridView(from);
  };

  onCheckAnswerClicked = () => {
    if (this.props.model.store.revealCorrectOnCheckAnswer) {
      this.setState({
        btnStatus: "seeCorrectAnswer"
      });
    }
    this.props.model.actions.checkAnswerClicked();
  };

  onSeeCorrectAnsClick = () => {
    this.setState({
      btnStatus: "yourResponse"
    });
    this.props.model.actions.revealCorrectAnswers();
  };

  onYourResponseClick = () => {
    this.setState({
      btnStatus: "checkAnswer"
    });
    this.props.model.actions.yourResponseClick();
  };

  onFinishTestClicked = () => {
    this.props.model.actions.finishTest(true);
  };

  onRetryTestClicked = () => {
    this.props.model.actions.onRetryTestClicked();
  };

  onCloseModal = () => {
    this.props.model.actions.onCloseAnAttemptedPopup();
  };

  backToScoreCard = () => {
    this.props.model.actions.backToScoreCard();
  };

  renderUnAnsweredPopup = unAnsweredQuestionLength => (
    <Popup
      header={{ title: "Submit this Assessment" }}
      onCloseClick={this.onCloseModal}
      popupStyle={{ maxHeight: "calc(100vh - 60px)", overflow: "auto" }}
      showPopup={true}
      withPortal={true}
    >
      <p className={unansweredTextStyle}>
        {unAnsweredQuestionLength} questions are unattempted.
      </p>
    </Popup>
  );

  renderScoreCard = () => {
    const {
      isMobile,
      reAttemptAllowedCount,
      reAttemptAllowed,
      cardState
    } = this.props.model.store;
    const { reAttemptCount } = cardState;
    let reAttemptsLeft = 0;
    if (reAttemptAllowedCount > -1) {
      reAttemptsLeft = reAttemptAllowedCount - reAttemptCount;
    } else if (reAttemptAllowedCount === -1) {
      reAttemptsLeft = 1;
    }
    return (
      <QuestionFooterGridViewContainer isMobile={isMobile}>
        <div
          style={{
            display: "flex",
            alignItems: "flex-start",
            justifyContent: "space-between",
            padding: isMobile ? "0px" : "0px 125px",
            width: "100%"
          }}
        >
          <style>{getGridIconStyle()}</style>
          {reAttemptAllowed && (
            <div>
              <SquareBorderButton
                onClick={this.onRetryTestClicked}
                style={{
                  width: "162px",
                  minWidth: "162px"
                }}
                icon={{
                  type: "retry",
                  style: { marginRight: "10px" }
                }}
                text="Retry"
                disabled={reAttemptsLeft <= 0}
              />
              {reAttemptAllowedCount > 0 && (
                <ReattemptsCount>
                  {reAttemptsLeft} attempts left
                </ReattemptsCount>
              )}
            </div>
          )}
          <SquareBorderButton
            onClick={this.onReviewClicked("footer")}
            style={{
              width: "173px",
              minWidth: "173px"
            }}
            icon={{
              type: "review",
              className: "review-icon-grid",
              style: { marginRight: "10px" }
            }}
            text="Review"
          />
        </div>
      </QuestionFooterGridViewContainer>
    );
  };

  renderTestFinishedGridViewFooter = () => {
    const {
      isMobile,
      cardState,
      reAttemptAllowedCount
    } = this.props.model.store;
    const { reAttemptCount } = cardState;
    let reAttemptsLeft = 0;
    if (reAttemptAllowedCount > -1) {
      reAttemptsLeft = reAttemptAllowedCount - reAttemptCount;
    } else if (reAttemptAllowedCount === -1) {
      reAttemptsLeft = 1;
    }
    return (
      <QuestionFooterContainer
        isMobile={isMobile}
        style={{ alignItems: "baseline" }}
      >
        <div>
          <SquareBorderButton
            onClick={this.backToScoreCard}
            style={{
              width: "142px",
              minWidth: "142px"
            }}
            text="Scorecard"
            icon={{
              type: "angle-left",
              style: { width: "18px", marginRight: "4px" }
            }}
          />
        </div>
      </QuestionFooterContainer>
    );
  };

  renderGridViewFooter = () => {
    const {
      isMobile,
      showUnAttemptedPopup,
      unAnsweredQuestionLength
    } = this.props.model.store;
    return (
      <QuestionFooterGridViewContainer isMobile={isMobile}>
        {showUnAttemptedPopup && unAnsweredQuestionLength
          ? this.renderUnAnsweredPopup(unAnsweredQuestionLength)
          : null}
        <SquareBorderButton
          onClick={this.onFinishTestClicked}
          style={{
            width: "133px",
            minWidth: "133px"
          }}
          text="Finish Test"
          type="filled"
        />
      </QuestionFooterGridViewContainer>
    );
  };

  renderDefaultFooter = () => {
    const {
      currentQuestionIndex,
      questionsLength,
      isMobile,
      showCheckAnswerBtn,
      disableCheckBtn,
      answerStatus,
      revealOnTestFinish,
      testFinished
    } = this.props.model.store;
    const isLastQuestion = currentQuestionIndex === questionsLength - 1;
    let btnStatus = this.state.btnStatus;
    revealOnTestFinish === "REVEAL" &&
      this.state.btnStatus === "checkAnswer" &&
      (btnStatus = "seeCorrectAnswer");
    return (
      <QuestionFooterContainer isMobile={isMobile}>
        <div>
          {showCheckAnswerBtn &&
          !testFinished &&
          btnStatus === "checkAnswer" ? (
            <SquareBorderButton
              onClick={this.onCheckAnswerClicked}
              text="Check Answer"
              css={{
                width: "162px",
                "@media(max-width: 375px)": {
                  width: "140px",
                  padding: "0 10px !important"
                }
              }}
              disabled={disableCheckBtn}
            />
          ) : null}
          {btnStatus === "seeCorrectAnswer" ? (
            <SquareBorderButton
              onClick={this.onSeeCorrectAnsClick}
              text="See Correct Answer"
              css={{
                width: "204px",
                "@media(max-width: 375px)": {
                  width: "180px",
                  padding: "0 10px !important"
                }
              }}
            />
          ) : null}
          {btnStatus === "yourResponse" ? (
            <SquareBorderButton
              onClick={this.onYourResponseClick}
              text="Your Response"
              css={{
                width: "170px",
                "@media(max-width: 375px)": {
                  width: "140px",
                  padding: "0 10px !important"
                }
              }}
            />
          ) : null}
          {btnStatus === "" ? (
            <SquareBorderButton
              text=""
              css={{
                width: "160px",
                opacity: "0 !important",
                pointerEvents: "none",
                "@media(max-width: 375px)": {
                  width: "140px",
                  padding: "0 10px !important"
                }
              }}
            />
          ) : null}
        </div>
        {!isMobile && answerStatus ? (
          <FooterFeedback
            isMobile={isMobile}
            isCorrect={answerStatus === "correct"}
          />
        ) : null}
        <div style={{ display: "flex" }}>
          <SquareBorderButton
            onClick={this.previousQuestionClicked}
            style={{
              width: "48px",
              minWidth: "48px",
              marginRight: "12px",
              padding: 0
            }}
            disabled={currentQuestionIndex < 1}
            text=""
            icon={{
              type: "angle-left",
              style: { width: "18px" }
            }}
          />
          {isLastQuestion ? (
            [
              <style>{getGridIconStyle()}</style>,
              <SquareBorderButton
                onClick={this.onReviewClicked("footer")}
                style={{
                  width: "126px",
                  minWidth: "126px"
                }}
                text="Review"
                iconPosition="right"
                icon={{
                  type: "review",
                  style: { width: "18px", marginLeft: "6px", fill: "inherit" }
                }}
                type="filled"
              />
            ]
          ) : (
            <SquareBorderButton
              onClick={this.nextQuestionClicked}
              style={{
                width: "113px",
                minWidth: "113px"
              }}
              text="Next"
              iconPosition="right"
              disabled={currentQuestionIndex === questionsLength - 1}
              icon={{
                type: "angle-right",
                style: { width: "18px" }
              }}
              type="filled"
            />
          )}
        </div>
      </QuestionFooterContainer>
    );
  };

  render() {
    const { gridView, showScoreCard, testFinished } = this.props.model.store;
    if (showScoreCard) {
      return <React.Fragment>{this.renderScoreCard()}</React.Fragment>;
    }
    if (testFinished && gridView) {
      return (
        <React.Fragment>
          {this.renderTestFinishedGridViewFooter()}
        </React.Fragment>
      );
    }
    if (gridView) {
      return <React.Fragment>{this.renderGridViewFooter()}</React.Fragment>;
    }
    return <React.Fragment>{this.renderDefaultFooter()}</React.Fragment>;
  }
}

export default QuestionFooter;
