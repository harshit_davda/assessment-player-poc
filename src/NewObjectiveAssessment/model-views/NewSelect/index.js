import React, { Component } from 'react';
import { observer } from 'mobx-react';
import QuestionText from './components/QuestionText';
import OptionItem from './components/OptionItem';
import { getObjectiveResponseType } from '../../helpers';

@observer
class NewSelect extends Component {
  constructor(props) {
    super(props);
    const { currentQuestion, currentQuestionState } = props.model.store;
    this.state = {
      selectedOptions: this.getSelectedIndicies(currentQuestion, currentQuestionState)
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { currentQuestion, currentQuestionState } = nextProps.model.store;
    const selectedOptions = this.getSelectedIndicies(currentQuestion, currentQuestionState);
    this.setState({
      selectedOptions
    });
  }

  onInputChange = index => () => {
    const { currentQuestion, testFinished } = this.props.model.store;
    if (!testFinished) {
      let selectedOptions = [...this.state.selectedOptions];
      const responseType = getObjectiveResponseType(currentQuestion);
      if (responseType === 'MULTIPLE_SELECT') {
        selectedOptions[index] = !selectedOptions[index];
      } else if (responseType === 'SINGLE_SELECT') {
        selectedOptions = [];
        selectedOptions[index] = true;
      }
      this.props.model.actions.updateSelectedOptions(selectedOptions);
      this.setState({ selectedOptions });
    }
  };

  getSelectedIndicies = (questionOffer, questionState) => {
    const submission = questionState.submissions ? questionState.submissions[0] : null;
    const response = submission && submission.response ? submission.response : '';
    if (!response) {
      return [];
    }
    const options = questionOffer.content.question.options;
    const selectedIndices = response.split(',').map(index => parseInt(index, 10));
    return options.map((item, index) => selectedIndices.includes(index));
  };

  getSelectionStatus = (item, selectedOptions, index) => {
    const { updateAnswerStatus } = this.props.model.actions;
    const { checkAnswer, revealOnTestFinish } = this.props.model.store;
    if (selectedOptions[index]) {
      if (selectedOptions[index] === item.optionEvaluation) {
        return 'correct';
      }
      (checkAnswer || revealOnTestFinish) && updateAnswerStatus('wrong');
      return 'wrong';
    }
    if (selectedOptions[index] !== item.optionEvaluation) {
      (checkAnswer || revealOnTestFinish) && updateAnswerStatus('wrong');
    }
    return 'default';
  };

  getCorrectAnswers = (options) => {
    const correctAnswers = [];
    const optionsArr = options || [];
    optionsArr.forEach((item) => {
      correctAnswers.push(item.optionEvaluation);
    });
    return correctAnswers;
  };

  formRevealCorrectData = (correctAnswers, selectedOptions) => {
    const returnData = [];
    correctAnswers.forEach((item, index) => {
      if (item && selectedOptions[index]) {
        returnData.push(item);
      } else if (selectedOptions[index]) {
        returnData.push(selectedOptions[index]);
      } else {
        returnData.push(false);
      }
    });
    return returnData;
  };

  render() {
    const {
      currentQuestion,
      theme,
      checkAnswer,
      seeCorrectAnswers,
      revealOnTestFinish,
      seeActualAnswers,
      isMobile,
      isGradable,
      testFinished
    } = this.props.model.store;
    const { updateAnswerStatus, disableCheckBtnStatus } = this.props.model.actions;
    let { selectedOptions } = this.state;
    const { maxScore } = currentQuestion;
    const { questionText, options = [] } = currentQuestion.content.question;
    const isSingleSelect = getObjectiveResponseType(currentQuestion) === 'SINGLE_SELECT';
    const correctAnswers = this.getCorrectAnswers(options);
    if (
      revealOnTestFinish === 'REVEAL'
      && !seeCorrectAnswers
      && !checkAnswer
      && !seeActualAnswers
    ) {
      selectedOptions = this.formRevealCorrectData(correctAnswers, selectedOptions);
    } else {
      selectedOptions = seeCorrectAnswers ? correctAnswers : selectedOptions;
    }
    if (this.state.selectedOptions.length === correctAnswers.length) {
      disableCheckBtnStatus(false);
    } else {
      disableCheckBtnStatus(true);
      (checkAnswer || revealOnTestFinish) && updateAnswerStatus('wrong');
    }
    return (
      <div>
        <QuestionText
          questionText={questionText}
          marks={maxScore}
          theme={theme}
          isMobile={isMobile}
          isGradable={isGradable}
        />
        {options.map((item, index) => {
          const optionText = item.optionText;
          const selectionStatus = this.getSelectionStatus(item, selectedOptions, index);
          const isSelected = !!selectedOptions[index];
          return (
            <OptionItem
              optionText={optionText}
              key={optionText}
              theme={theme}
              index={index}
              isSingleSelect={isSingleSelect}
              onInputChange={this.onInputChange}
              isSelected={isSelected}
              checkAnswer={checkAnswer || revealOnTestFinish}
              selectionStatus={selectionStatus}
              seeCorrectAnswers={seeCorrectAnswers}
              isMobile={isMobile}
              testFinished={testFinished}
            />
          );
        })}
      </div>
    );
  }
}

export default NewSelect;
