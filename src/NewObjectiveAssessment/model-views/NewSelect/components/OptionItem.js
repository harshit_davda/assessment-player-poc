/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/no-danger */
import React, { Component } from "react";
import { Radio, Checkbox } from "@lyearn-team/frontend-components";
import styled from "react-emotion";

const colorObj = {
  correct: {
    color: "green",
    bg: "#f5fff6"
  },
  default: {
    color: "black3",
    bg: "#fff"
  },
  wrong: {
    color: "red",
    bg: "#fff5f5"
  },
  selected: {
    color: "accent",
    bg: "#fff"
  }
};

const OptionWrapper = styled.div`
  display: flex;
  align-items: center;
  font-family: ProximaNova;
  font-size: 16px;
  margin: 2px 0px;
  ${props =>
    !props.testFinished &&
    `
    &:hover {
      background-color: #f5f9ff;
    }
    `}
  ${props =>
    props.isSelected
      ? `
    background-color: #f5f9ff;
    color: ${props.theme.color.accent};
  `
      : `
    color: ${props.theme.color.black1};
  `};
  ${({ isMobile }) => `
    padding: ${isMobile ? "10px 20px" : "10px 50px"};
  `}
  .optionText {
    margin-left: 24px;
    display: flex;
    align-items: center;
  }
  ${props =>
    props.selectionStatus
      ? `
    background-color: ${colorObj[props.selectionStatus].bg};
    color: ${props.theme.color[colorObj[props.selectionStatus].color]};
    `
      : ""};
`;

const getSelectStyle = theme => {
  const styleStr = `
    .wrong input:checked ~ div {
      border-color: ${theme.color[colorObj.wrong.color]}
    }
    .checkbox-item.wrong input:checked ~ div {
      background-color: ${theme.color[colorObj.wrong.color]}
    }
    .wrong input:checked ~ div span {
      background-color: ${theme.color[colorObj.wrong.color]}
    }
    .correct input:checked ~ div {
      border-color: ${theme.color[colorObj.correct.color]}
    }
    .unselected-correct-option input:checked ~ div {
      border-color: ${theme.color[colorObj.correct.color]}
    }
    .checkbox-item.correct input:checked ~ div {
      background-color: ${theme.color[colorObj.correct.color]}
    }
    .correct input:checked ~ div span {
      background-color: ${theme.color[colorObj.correct.color]}
    }
  `;
  return styleStr;
};

class OptionItem extends Component {
  render() {
    const {
      optionText,
      theme,
      isSingleSelect,
      index,
      onInputChange,
      isSelected,
      checkAnswer,
      selectionStatus,
      seeCorrectAnswers,
      isMobile,
      testFinished
    } = this.props;
    const InputComponent = isSingleSelect ? Radio : Checkbox;
    const propsToPass = {
      theme,
      isSelected,
      isMobile,
      testFinished
    };
    if (checkAnswer || seeCorrectAnswers) {
      propsToPass.selectionStatus = selectionStatus;
    }
    const displayClass = checkAnswer
      ? selectionStatus
      : seeCorrectAnswers
      ? selectionStatus
      : "";
    return (
      <label>
        <OptionWrapper
          {...propsToPass}
          className={`${displayClass} ${
            isSingleSelect ? "radio-item" : "checkbox-item"
          }`}
        >
          <style>{getSelectStyle(theme)}</style>
          <InputComponent
            checked={isSelected}
            onChange={onInputChange(index)}
          />
          <div
            className="optionText"
            dangerouslySetInnerHTML={{
              __html: optionText
            }}
          />
        </OptionWrapper>
      </label>
    );
  }
}

export default OptionItem;
