import React from "react";
import styled from "react-emotion";

const QuestionText = styled.div`
  font-family: ProximaNova;
  font-size: 19px;
  margin-bottom: 20px;
  line-height: 28px;
  ${({ theme: { color } }) => `
    color: ${color.black1};
  `}
  ${({ isMobile }) => `
    padding: ${isMobile ? "0 20px" : "0 50px"};
  `}
  .marksText {
    font-family: ProximaNova;
    font-size: 16px;
    ${({ theme: { color } }) => `
    color: ${color.black2};
    margin-left: 6px;
    min-width: 100px;
    margin-top: 1px;
  `}
  }
`;

function QuestionTextWrapper({
  theme,
  questionText,
  marks,
  isMobile,
  isGradable
}) {
  const transformedQuestionText = questionText;
  return (
    <QuestionText
      isMobile={isMobile}
      theme={theme}
      dangerouslySetInnerHTML={{
        __html: transformedQuestionText
      }}
    />
  );
}

export default QuestionTextWrapper;
