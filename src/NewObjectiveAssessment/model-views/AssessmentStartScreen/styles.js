import styled from "react-emotion";

// overflow: auto;
export const Wrapper = styled.div`
  display: block;
  ${({ isMobile }) => `
    overflow: auto;
    padding: ${isMobile ? "45px 20px" : "45px 70px"};
  `}
  @media (min-width: 767px) and (max-height: 700px) {
    padding: 5px 70px;
  }
`;

export const HeaderText = styled.div`
  font-family: ProximaNova;
  text-align: center;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  ${props => `
    color: ${props.theme.color.black1};
    font-size: ${props.isMobile ? "24px" : "34px"};
    margin-bottom: ${props.isMobile ? "5px" : "34px"};
  `}
  @media (min-width: 767px) and (max-height: 700px) {
    font-size: 24px;
    margin-bottom: 5px;
    margin-top: 10px;
  }
`;

export const TestInfo = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  ${({ isMobile }) => `
    padding: ${isMobile ? "20px 0 0" : "21px 0"};
  `}
  @media (min-width: 767px) and (max-height: 700px) {
    padding: 5px 0px;
  }
`;

export const Info = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  ${({ isMobile }) => `
    width: ${isMobile ? "50%" : "25%"};
    margin-bottom: ${isMobile ? "22px" : "unset"};
  `}
`;

export const InfoHeader = styled.div`
  font-family: ProximaNova;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.5;
  letter-spacing: normal;
  margin-bottom: 5px;
  ${props => `
    color: ${props.theme.color.black2}
  `}
`;

export const InfoValue = styled.div`
  font-family: ProximaNova;
  font-size: 24px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1;
  letter-spacing: normal;
  color: #2c90e9;
`;

export const InstructionsScrollableWrapper = styled.div`
  overflow: auto;
  margin: 20px 0px 0px;

  @media (min-width: 767px) and (max-height: 700px) {
    margin: 5px 0px 0px;
    max-height: calc(100vh - 410px);
  }
`;

export const InstructionsWrapper = styled.div`
  height: auto;
  border-radius: 10px;
  background-color: #f5f9ff;
  width: 100%;
  max-width: 643px;
  ${({ isMobile }) => `
    margin: ${isMobile ? "5px 0 25px" : "34px auto 41px"};
    padding: ${isMobile ? "15px 15px 1px" : "35px 35px 17px"};
  `}
  @media (min-width: 767px) and (max-height: 700px) {
    padding: 5px 35px 10px;
    margin: 0 auto 10px;
  }
`;

export const InstructionsText = styled.div`
  font-family: ProximaNova;
  font-size: 12px;
  font-weight: 600;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: 0.9px;
  margin-bottom: 18px;
  text-transform: uppercase;
  ${props => `
    color: ${props.theme.color.accent}
  `}
`;

export const InstructionWrapper = styled.div`
  display: flex;
  align-items: baseline;
  ${({ isMobile }) => `
    margin-bottom: ${isMobile ? "14px" : "18px"};
  `}
`;

export const BulletPoint = styled.div`
  min-width: 5px;
  width: 5px;
  height: 5px;
  border-radius: 50%;
  margin-right: 14px;
  ${props => `
    background-color: ${props.theme.color.black2}
  `}
`;

export const Instruction = styled.div`
  font-family: ProximaNova;
  font-size: 14px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  ${props => `
    color: ${props.theme.color.black2}
  `}
`;

export const BottomContainer = styled.div`
  left: 0px;
  bottom: 0px;
  width: 100%;
  padding: 20px 20px;
  background: #fff;
  position: absolute;
  ${props => `
    border-top: 1px solid ${props.theme.color.grey2};
  `}
  @media (min-width: 767px) and (max-height: 700px) {
    padding: 5px 20px;
  }
`;
