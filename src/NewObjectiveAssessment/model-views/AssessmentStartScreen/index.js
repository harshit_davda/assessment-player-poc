import React, { Component } from "react";
import { observer } from "mobx-react";
import { Divider, SquareBorderButton } from "@lyearn-team/frontend-components";
import { marksUptoTwoDecimal } from "../../helpers";
import {
  Wrapper,
  HeaderText,
  TestInfo,
  Info,
  InfoHeader,
  InfoValue,
  InstructionsScrollableWrapper,
  InstructionsWrapper,
  InstructionsText,
  Instruction,
  BulletPoint,
  InstructionWrapper,
  BottomContainer
} from "./styles";

@observer
class AssessmentStartScreen extends Component {
  onStartTest = () => {
    this.props.model.actions.startTest();
  };

  render() {
    const {
      card,
      questionsLength,
      isMobile,
      cardState
    } = this.props.model.store;
    const {
      maxScore = 0,
      minScaledScore = 0,
      timeBoundTest,
      reAttemptAllowedCount,
      penaltyMarks,
      content: { duration }
    } = card;
    const { reAttemptCount } = cardState;
    // const hours = parseInt(duration / 3600, 10);
    const minutes = parseInt((duration % 3600) / 60, 10);
    // const seconds = parseInt(duration % 60, 10);
    let reAttemptsLeft = 0;
    if (reAttemptAllowedCount === -1) {
      reAttemptsLeft = "Unlimited";
    } else {
      reAttemptsLeft = 1 + (reAttemptAllowedCount - reAttemptCount);
    }
    const durationString = `${minutes} minutes`;
    const durationShort = `${minutes}m`;
    return (
      <Wrapper isMobile={isMobile}>
        <HeaderText isMobile={isMobile}>
          Grade 4 End of Year Assessment
        </HeaderText>
        {isMobile ? null : <Divider />}
        <TestInfo isMobile={isMobile}>
          <Info isMobile={isMobile}>
            <InfoHeader>Duration</InfoHeader>
            <InfoValue>{durationShort}</InfoValue>
          </Info>
          <Info isMobile={isMobile}>
            <InfoHeader>Passing Score</InfoHeader>
            <InfoValue>
              {marksUptoTwoDecimal(maxScore * minScaledScore)}
            </InfoValue>
          </Info>
          <Info isMobile={isMobile}>
            <InfoHeader>Maximum Score</InfoHeader>
            <InfoValue>{marksUptoTwoDecimal(maxScore)}</InfoValue>
          </Info>
          <Info isMobile={isMobile}>
            <InfoHeader>Questions</InfoHeader>
            <InfoValue>{questionsLength}</InfoValue>
          </Info>
        </TestInfo>
        {isMobile ? null : <Divider />}
        <InstructionsScrollableWrapper isMobile={isMobile}>
          <InstructionsWrapper isMobile={isMobile}>
            <InstructionsText>Instructions</InstructionsText>
            <InstructionWrapper isMobile={isMobile}>
              <BulletPoint />
              <Instruction>
                You have {reAttemptsLeft} attempts to pass this assessment.
              </Instruction>
            </InstructionWrapper>
            {timeBoundTest && (
              <InstructionWrapper isMobile={isMobile}>
                <BulletPoint />
                <Instruction>
                  Once you start, you have {durationString} time to complete.
                </Instruction>
              </InstructionWrapper>
            )}
            {penaltyMarks && (
              <InstructionWrapper isMobile={isMobile}>
                <BulletPoint />
                <Instruction>
                  For each incorrect answer, there is a penalty of
                  {marksUptoTwoDecimal(penaltyMarks)} marks.
                </Instruction>
              </InstructionWrapper>
            )}
          </InstructionsWrapper>
        </InstructionsScrollableWrapper>
        <BottomContainer isMobile={isMobile}>
          <SquareBorderButton
            onClick={this.onStartTest}
            style={{
              width: "169px",
              minWidth: "169px",
              margin: "auto",
              minHeight: "44px",
              left: "50%",
              position: "relative",
              transform: "translateX(-50%)"
            }}
            text="Start Test"
            type="filled"
            disabled={reAttemptsLeft <= 0}
          />
        </BottomContainer>
      </Wrapper>
    );
  }
}

export default AssessmentStartScreen;
