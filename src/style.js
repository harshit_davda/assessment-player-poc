import { css } from "emotion";

export const appRoot = css`
  position: fixed;
  width: 800px;
  overflow: hidden;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);

  @media (max-width: 767px) {
    width: 100%;
  }
`;
