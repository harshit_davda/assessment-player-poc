const props = {
  theme: {
    color: {
      accent: "#4a90e2",
      accentHover: "#6eacf5",
      accentClicked: "#256fc5",
      accentHighlight: "#e9f3ff",
      accentDark: "#1156a6",
      accentBackground: "#f0f7ff",
      heading: "#222f42",
      backgroundPrimary: "#e8ebf1",
      backgroundSecondary: "#d2d5da",
      black1: "#223142",
      black2: "#707a85",
      black3: "#929fae",
      grey1: "#c5d0de",
      grey2: "#dfe6ee",
      grey3: "#eaf0f8",
      grey4: "#f2f6fc",
      white: "#fff",
      red: "#f85359",
      redHover: "#ff8084",
      redHighlight: "#fff0f0",
      redClicked: "#e32d34",
      redDark: "#cc1f25",
      redBackground: "#fff5f5",
      yellow: "#f29924",
      yellowHover: "#ffb24d",
      green: "#39b54a",
      greenHover: "#54d566",
      greenHighlight: "#e6fce9",
      greenClicked: "#198c29",
      greenDark: "#0a7319",
      greenBackground: "#f0fff2",
      orangeMedium: "#d97b00",
      orangeLight: "#fff1de",
      orangeBackground: "#fff8f0",
      dark: {
        uranus: "#070a0d",
        saturn: "#14181e",
        jupiter: "#181e26",
        mars: "#222933",
        earth: "#2c343f",
        venus: "#36404c",
        mercury: "#424c59",
        sapphire: "#1c62fd",
        amethyst: "#4780ff",
        ruby: "#ff4646",
        topaz: "#decd27",
        emerald: "#1cfda2",
        iron: "#898989",
        copper: "#d0d0d0",
        silver: "#e3e3e3",
        zinc: "#686b6e",
        cobalt: "#999ca1",
        nickel: "#e3e3e3",
        marigold: "#ffaf5f",
        zinnia: "#4acfac",
        lavandula: "#7e8ce0",
        dandelion: "#36c7d0",
        calendula: "#ffa48e",
        gardenia: "#ffdcb3",
        daffodil: "#96eb91",
        orchid: "#af43cb",
        white: "#fff",
        white0_06: "rgba(255, 255, 255, 0.06)",
        white0_12: "rgba(255, 255, 255, 0.12)",
        white0_18: "rgba(255, 255, 255, 0.18)",
        white0_1: "rgba(255, 255, 255, 0.1)",
        black0_1: "rgba(0, 0, 0, 0.1)",
        sapphireHover: "#3471fd",
        sapphireFocus: "#1958e3",
        disabledOpacity: 0.4
      },
      separator: "#d2d5da",
      text: "#223142"
    },
    staticColor: {
      learnerIndexActiveBg: "#515e71",
      learnerIndexBg: "#152338",
      learnerIndexLine: "#2b3848"
    },
    font: {
      contentFont: "ChronicaPro"
    },
    disabledOpacity: 0.5
  },
  cardData: {
    card: {
      _id: "5e32bf945f63ade9c18151ce",
      groupIds: [],
      dynamicGroupIds: [],
      customProperties: [],
      versionLogData: {
        _id: "5e32d29895e120857c69645d"
      },
      questionSettings: {
        penaltyMarks: 0,
        answerAttemptsAllowed: 2,
        allowCheckAnswer: true,
        revealCorrectOnCheckAnswer: false
      },
      orderOfCompletion: "anyOrder",
      deleted: false,
      progressWeight: 1,
      childCompanyIds: [],
      minProgressMeasure: 1,
      contributeToCompleteStatus: true,
      contributeToSuccessStatus: true,
      modelType: "offer",
      publishedVersion: 1,
      version: 0,
      curriculumOfferIds: [],
      coAdminIds: [],
      autoAssignUserGroupIds: [],
      offerType: "assignmentCard",
      content: {
        modelType: "content",
        childrenIds: [],
        _id: "5e32d29895e120515a69645e",
        contentType: "assignmentCard",
        name: "Assignment#1.6",
        subType: "objective",
        duration: 600,
        hidden: false,
        globalPosition: "1.6",
        companyId: "56bfef41a17441a959d9c8da",
        defaultDuration: true,
        blockInfo: []
      },
      completionType: "completionFromContent",
      reAttemptAllowedCount: 1,
      rootOfferId: "5e268a86bee9d9455beb0932",
      isGradable: true,
      reAttemptAllowed: true,
      completionStatusRollupRules: [
        {
          _id: "5e32d29895e1209134696461",
          action: "complete",
          formula: {
            quantifier: "all"
          },
          requiredCondition: "complete"
        },
        {
          _id: "5e32d29895e1205a4d696460",
          action: "inComplete",
          formula: {
            quantifier: "any"
          },
          requiredCondition: "inComplete"
        },
        {
          _id: "5e32d29895e120b8f469645f",
          action: "notAttempted",
          formula: {
            quantifier: "all"
          },
          requiredCondition: "notAttempted"
        }
      ],
      children: [
        {
          cardQuestion: {
            _id: "5e32bfcb5f63ade9c18151cf",
            groupIds: [],
            dynamicGroupIds: [],
            customProperties: [],
            versionLogData: {
              _id: "5e32d29895e12027ad69647e"
            },
            orderOfCompletion: "anyOrder",
            deleted: false,
            progressWeight: 1,
            childCompanyIds: [],
            minProgressMeasure: 1,
            contributeToCompleteStatus: true,
            contributeToSuccessStatus: false,
            modelType: "offer",
            publishedVersion: 1,
            version: 0,
            curriculumOfferIds: [],
            coAdminIds: [],
            autoAssignUserGroupIds: [],
            offerType: "cardQuestion",
            children: [],
            completionCriteria: "minProgressMeasure",
            isGradable: true,
            minScore: 0,
            maxScore: 10,
            rootOfferId: "5e268a86bee9d9455beb0932",
            content: {
              modelType: "content",
              childrenIds: [],
              _id: "5e32d29895e120acd269647f",
              companyId: "56bfef41a17441a959d9c8da",
              contentType: "cardQuestion",
              gradingScheme: "autoGradable",
              question: {
                responseType: ["singleSelect"],
                questionText: "<p>Estimate the quotient for 417 ÷ 6</p>",
                options: [
                  {
                    optionText: "<p>About 7</p>",
                    optionEvaluation: false
                  },
                  {
                    optionText: "<p>About 50</p>",
                    optionEvaluation: false
                  },
                  {
                    optionText: "<p>About 70</p>",
                    optionEvaluation: true
                  },
                  {
                    optionText: "<p>About 80</p>",
                    optionEvaluation: false
                  }
                ]
              },
              blockInfo: []
            },
            completionStatusRollupRules: [],
            successStatusRollupRules: [],
            prerequisites: [],
            peerQuestions: []
          },
          state: {
            contentType: "cardQuestion",
            userId: "5b8ec10ffd9b940019fa084e",
            rootContentId_contentId:
              "5e268a86bee9d9455beb0932_5e32bfcb5f63ade9c18151cf",
            companyId: "56bfef41a17441a959d9c8da",
            contentId: "5e32bfcb5f63ade9c18151cf",
            rawScore: 0,
            scaledScore: 0,
            progressMeasure: 0,
            scaledProgressMeasure: 0,
            completionStatus: "notAttempted",
            successStatus: "na",
            forceComplete: false,
            timeSpent: 0,
            submissionCount: 0,
            lastUpdatedTimeSpent: "1000-01-01T00:00:00.000Z",
            lastSubmittedOn: "1000-01-01T00:00:00.000Z",
            createdAt: "2020-01-30T12:58:44.579Z",
            updatedAt: "2020-01-30T12:58:44.579Z",
            startedOn: "1000-01-01T00:00:00.000Z",
            lastAccessedOn: "1000-01-01T00:00:00.000Z",
            successOn: "1000-01-01T00:00:00.000Z",
            completedOn: "1000-01-01T00:00:00.000Z",
            versionNumber: 0,
            submissions: [
              {
                _id: "b71b0586-2530-40a2-9630-2da3b1f89757",
                rawScore: 0,
                gradedBy: null,
                progressMeasure: 0,
                response: "",
                answerEvaluation: "",
                responseType: "",
                submittedOn: "1000-01-01T00:00:00.000Z",
                reviewsReceivedCount: 0
              }
            ],
            reAttemptCount: 0,
            actualSubmit: false,
            userId_contentId:
              "5b8ec10ffd9b940019fa084e_5e32bfcb5f63ade9c18151cf",
            userId_offerId: "5b8ec10ffd9b940019fa084e_5e268a86bee9d9455beb0932"
          }
        },
        {
          cardQuestion: {
            _id: "5e32c04b5f63ade9c18151d0",
            groupIds: [],
            dynamicGroupIds: [],
            customProperties: [],
            versionLogData: {
              _id: "5e32d29895e12010fe69647b"
            },
            orderOfCompletion: "anyOrder",
            deleted: false,
            progressWeight: 1,
            childCompanyIds: [],
            minProgressMeasure: 1,
            contributeToCompleteStatus: true,
            contributeToSuccessStatus: false,
            modelType: "offer",
            publishedVersion: 1,
            version: 0,
            curriculumOfferIds: [],
            coAdminIds: [],
            autoAssignUserGroupIds: [],
            offerType: "cardQuestion",
            children: [],
            completionCriteria: "minProgressMeasure",
            isGradable: true,
            minScore: 0,
            maxScore: 10,
            rootOfferId: "5e268a86bee9d9455beb0932",
            content: {
              modelType: "content",
              childrenIds: [],
              _id: "5e32d29895e120b59369647c",
              companyId: "56bfef41a17441a959d9c8da",
              contentType: "cardQuestion",
              gradingScheme: "autoGradable",
              question: {
                responseType: ["singleSelect"],
                questionText:
                  "<span>Which shows 43,768 rounded to the nearest hundred?</span>",
                options: [
                  {
                    optionText: "<p>43,700</p>",
                    optionEvaluation: false
                  },
                  {
                    optionText: "<p>43,770</p>",
                    optionEvaluation: false
                  },
                  {
                    optionText: "<p>43,800</p>",
                    optionEvaluation: true
                  },
                  {
                    optionText: "<p>44,000</p>",
                    optionEvaluation: false
                  }
                ]
              },
              blockInfo: []
            },
            completionStatusRollupRules: [],
            successStatusRollupRules: [],
            prerequisites: [],
            peerQuestions: []
          },
          state: {
            contentType: "cardQuestion",
            userId: "5b8ec10ffd9b940019fa084e",
            rootContentId_contentId:
              "5e268a86bee9d9455beb0932_5e32c04b5f63ade9c18151d0",
            companyId: "56bfef41a17441a959d9c8da",
            contentId: "5e32c04b5f63ade9c18151d0",
            rawScore: 0,
            scaledScore: 0,
            progressMeasure: 0,
            scaledProgressMeasure: 0,
            completionStatus: "notAttempted",
            successStatus: "na",
            forceComplete: false,
            timeSpent: 0,
            submissionCount: 0,
            lastUpdatedTimeSpent: "1000-01-01T00:00:00.000Z",
            lastSubmittedOn: "1000-01-01T00:00:00.000Z",
            createdAt: "2020-01-30T12:58:44.579Z",
            updatedAt: "2020-01-30T12:58:44.579Z",
            startedOn: "1000-01-01T00:00:00.000Z",
            lastAccessedOn: "1000-01-01T00:00:00.000Z",
            successOn: "1000-01-01T00:00:00.000Z",
            completedOn: "1000-01-01T00:00:00.000Z",
            versionNumber: 0,
            submissions: [
              {
                _id: "a77dc3cc-8d4e-42bb-aa9d-d827b3231c27",
                rawScore: 0,
                gradedBy: null,
                progressMeasure: 0,
                response: "",
                answerEvaluation: "",
                responseType: "",
                submittedOn: "1000-01-01T00:00:00.000Z",
                reviewsReceivedCount: 0
              }
            ],
            reAttemptCount: 0,
            actualSubmit: false,
            userId_contentId:
              "5b8ec10ffd9b940019fa084e_5e32c04b5f63ade9c18151d0",
            userId_offerId: "5b8ec10ffd9b940019fa084e_5e268a86bee9d9455beb0932"
          }
        },
        {
          cardQuestion: {
            _id: "5e32c5d25f63ade9c18151e2",
            groupIds: [],
            dynamicGroupIds: [],
            customProperties: [],
            versionLogData: {
              _id: "5e32d29895e120d8f0696478"
            },
            orderOfCompletion: "anyOrder",
            deleted: false,
            progressWeight: 1,
            childCompanyIds: [],
            minProgressMeasure: 1,
            contributeToCompleteStatus: true,
            contributeToSuccessStatus: false,
            modelType: "offer",
            publishedVersion: 1,
            version: 0,
            curriculumOfferIds: [],
            coAdminIds: [],
            autoAssignUserGroupIds: [],
            offerType: "cardQuestion",
            children: [],
            completionCriteria: "minProgressMeasure",
            isGradable: true,
            minScore: 0,
            maxScore: 10,
            rootOfferId: "5e268a86bee9d9455beb0932",
            content: {
              modelType: "content",
              childrenIds: [],
              _id: "5e32d29895e12000ac696479",
              companyId: "56bfef41a17441a959d9c8da",
              contentType: "cardQuestion",
              gradingScheme: "autoGradable",
              question: {
                responseType: ["singleSelect"],
                questionText:
                  '<p>Charlotte has a piece of yarn in her craft kit. She uses <sup>3</sup>&frasl;<sub>8</sub> of the length of the yarn to make a bookmark. What part of the yarn is left?</p><br><img src="https://dndw32r4edmw5.cloudfront.net/assets/frontend-js/fonts/assessment-player/5e32c5d25f63ade9c18151e2.png" alt="Figure" />',
                options: [
                  {
                    optionText:
                      "<span class='fraction'><span class='top'>1</span><span class='bottom'>8</span></span>",
                    optionEvaluation: false
                  },
                  {
                    optionText:
                      "<span class='fraction'><span class='top'>3</span><span class='bottom'>8</span></span>",
                    optionEvaluation: false
                  },
                  {
                    optionText:
                      "<span class='fraction'><span class='top'>5</span><span class='bottom'>8</span></span>",
                    optionEvaluation: true
                  },
                  {
                    optionText:
                      "<span class='fraction'><span class='top'>8</span><span class='bottom'>8</span></span>",
                    optionEvaluation: false
                  }
                ]
              },
              blockInfo: []
            },
            completionStatusRollupRules: [],
            successStatusRollupRules: [],
            prerequisites: [],
            peerQuestions: []
          },
          state: {
            contentType: "cardQuestion",
            userId: "5b8ec10ffd9b940019fa084e",
            rootContentId_contentId:
              "5e268a86bee9d9455beb0932_5e32c5d25f63ade9c18151e2",
            companyId: "56bfef41a17441a959d9c8da",
            contentId: "5e32c5d25f63ade9c18151e2",
            rawScore: 0,
            scaledScore: 0,
            progressMeasure: 0,
            scaledProgressMeasure: 0,
            completionStatus: "notAttempted",
            successStatus: "na",
            forceComplete: false,
            timeSpent: 0,
            submissionCount: 0,
            lastUpdatedTimeSpent: "1000-01-01T00:00:00.000Z",
            lastSubmittedOn: "1000-01-01T00:00:00.000Z",
            createdAt: "2020-01-30T12:58:44.579Z",
            updatedAt: "2020-01-30T12:58:44.579Z",
            startedOn: "1000-01-01T00:00:00.000Z",
            lastAccessedOn: "1000-01-01T00:00:00.000Z",
            successOn: "1000-01-01T00:00:00.000Z",
            completedOn: "1000-01-01T00:00:00.000Z",
            versionNumber: 0,
            submissions: [
              {
                _id: "18a42049-2482-45b7-9172-5daed6982385",
                rawScore: 0,
                gradedBy: null,
                progressMeasure: 0,
                response: "",
                answerEvaluation: "",
                responseType: "",
                submittedOn: "1000-01-01T00:00:00.000Z",
                reviewsReceivedCount: 0
              }
            ],
            reAttemptCount: 0,
            actualSubmit: false,
            userId_contentId:
              "5b8ec10ffd9b940019fa084e_5e32c5d25f63ade9c18151e2",
            userId_offerId: "5b8ec10ffd9b940019fa084e_5e268a86bee9d9455beb0932"
          }
        },
        {
          cardQuestion: {
            _id: "5e32c7c05f63ade9c18151e4",
            groupIds: [],
            dynamicGroupIds: [],
            customProperties: [],
            versionLogData: {
              _id: "5e32d29895e1209831696475"
            },
            orderOfCompletion: "anyOrder",
            deleted: false,
            progressWeight: 1,
            childCompanyIds: [],
            minProgressMeasure: 1,
            contributeToCompleteStatus: true,
            contributeToSuccessStatus: false,
            modelType: "offer",
            publishedVersion: 1,
            version: 0,
            curriculumOfferIds: [],
            coAdminIds: [],
            autoAssignUserGroupIds: [],
            offerType: "cardQuestion",
            children: [],
            completionCriteria: "minProgressMeasure",
            isGradable: true,
            minScore: 0,
            maxScore: 10,
            rootOfferId: "5e268a86bee9d9455beb0932",
            content: {
              modelType: "content",
              childrenIds: [],
              _id: "5e32d29895e120ac82696476",
              companyId: "56bfef41a17441a959d9c8da",
              contentType: "cardQuestion",
              gradingScheme: "autoGradable",
              question: {
                responseType: ["singleSelect"],
                questionText:
                  "<p>Find n. Then find the perimeter of the figure shown.</p><p>Area = 48 square meters<br></p><br><img src='https://dndw32r4edmw5.cloudfront.net/assets/frontend-js/fonts/assessment-player/5e32c7c05f63ade9c18151e4.png' alt='Figure' />",
                options: [
                  {
                    optionText: "<p><i>n</i> = 6 m; <i>P</i> = 36 m</p>",
                    optionEvaluation: false
                  },
                  {
                    optionText: "<p><i>n</i> = 8 m; <i>P</i> = 28 m</p>",
                    optionEvaluation: true
                  }
                ]
              },
              blockInfo: []
            },
            completionStatusRollupRules: [],
            successStatusRollupRules: [],
            prerequisites: [],
            peerQuestions: []
          },
          state: {
            contentType: "cardQuestion",
            userId: "5b8ec10ffd9b940019fa084e",
            rootContentId_contentId:
              "5e268a86bee9d9455beb0932_5e32c7c05f63ade9c18151e4",
            companyId: "56bfef41a17441a959d9c8da",
            contentId: "5e32c7c05f63ade9c18151e4",
            rawScore: 0,
            scaledScore: 0,
            progressMeasure: 0,
            scaledProgressMeasure: 0,
            completionStatus: "notAttempted",
            successStatus: "na",
            forceComplete: false,
            timeSpent: 0,
            submissionCount: 0,
            lastUpdatedTimeSpent: "1000-01-01T00:00:00.000Z",
            lastSubmittedOn: "1000-01-01T00:00:00.000Z",
            createdAt: "2020-01-30T12:58:44.579Z",
            updatedAt: "2020-01-30T12:58:44.579Z",
            startedOn: "1000-01-01T00:00:00.000Z",
            lastAccessedOn: "1000-01-01T00:00:00.000Z",
            successOn: "1000-01-01T00:00:00.000Z",
            completedOn: "1000-01-01T00:00:00.000Z",
            versionNumber: 0,
            submissions: [
              {
                _id: "acb87f48-e78a-4b88-8488-55aceead8ccb",
                rawScore: 0,
                gradedBy: null,
                progressMeasure: 0,
                response: "",
                answerEvaluation: "",
                responseType: "",
                submittedOn: "1000-01-01T00:00:00.000Z",
                reviewsReceivedCount: 0
              }
            ],
            reAttemptCount: 0,
            actualSubmit: false,
            userId_contentId:
              "5b8ec10ffd9b940019fa084e_5e32c7c05f63ade9c18151e4",
            userId_offerId: "5b8ec10ffd9b940019fa084e_5e268a86bee9d9455beb0932"
          }
        },
        {
          cardQuestion: {
            _id: "5e32cd795f63ade9c18151e5",
            groupIds: [],
            dynamicGroupIds: [],
            customProperties: [],
            versionLogData: {
              _id: "5e32d29895e1205d99696472"
            },
            orderOfCompletion: "anyOrder",
            deleted: false,
            progressWeight: 1,
            childCompanyIds: [],
            minProgressMeasure: 1,
            contributeToCompleteStatus: true,
            contributeToSuccessStatus: false,
            modelType: "offer",
            publishedVersion: 1,
            version: 0,
            curriculumOfferIds: [],
            coAdminIds: [],
            autoAssignUserGroupIds: [],
            offerType: "cardQuestion",
            children: [],
            completionCriteria: "minProgressMeasure",
            isGradable: true,
            minScore: 0,
            maxScore: 10,
            rootOfferId: "5e268a86bee9d9455beb0932",
            content: {
              modelType: "content",
              childrenIds: [],
              _id: "5e32d29895e1205a1a696473",
              companyId: "56bfef41a17441a959d9c8da",
              contentType: "cardQuestion",
              gradingScheme: "autoGradable",
              question: {
                responseType: ["multipleSelect"],
                questionText:
                  "<p>Which of the following statements are true? Select all that apply.</p><br><img src='https://dndw32r4edmw5.cloudfront.net/assets/frontend-js/fonts/assessment-player/5e32cd795f63ade9c18151e5.png' alt='Figure' />",
                options: [
                  {
                    optionText:
                      "<p>The line plot shows scores for 21 students.</p>",
                    optionEvaluation: false
                  },
                  {
                    optionText: "<p>The score 3 is an outlier.</p>",
                    optionEvaluation: true
                  },
                  {
                    optionText: "<p>The highest score is 8.</p>",
                    optionEvaluation: false
                  },
                  {
                    optionText:
                      "<p>The difference between the highest and lowest score is 8.</p>",
                    optionEvaluation: true
                  }
                ]
              },
              blockInfo: []
            },
            completionStatusRollupRules: [],
            successStatusRollupRules: [],
            prerequisites: [],
            peerQuestions: []
          },
          state: {
            contentType: "cardQuestion",
            userId: "5b8ec10ffd9b940019fa084e",
            rootContentId_contentId:
              "5e268a86bee9d9455beb0932_5e32cd795f63ade9c18151e5",
            companyId: "56bfef41a17441a959d9c8da",
            contentId: "5e32cd795f63ade9c18151e5",
            rawScore: 0,
            scaledScore: 0,
            progressMeasure: 0,
            scaledProgressMeasure: 0,
            completionStatus: "notAttempted",
            successStatus: "na",
            forceComplete: false,
            timeSpent: 0,
            submissionCount: 0,
            lastUpdatedTimeSpent: "1000-01-01T00:00:00.000Z",
            lastSubmittedOn: "1000-01-01T00:00:00.000Z",
            createdAt: "2020-01-30T12:58:44.580Z",
            updatedAt: "2020-01-30T12:58:44.580Z",
            startedOn: "1000-01-01T00:00:00.000Z",
            lastAccessedOn: "1000-01-01T00:00:00.000Z",
            successOn: "1000-01-01T00:00:00.000Z",
            completedOn: "1000-01-01T00:00:00.000Z",
            versionNumber: 0,
            submissions: [
              {
                _id: "fca27168-01bb-4bf8-9196-43dc7108b8cb",
                rawScore: 0,
                gradedBy: null,
                progressMeasure: 0,
                response: "",
                answerEvaluation: "",
                responseType: "",
                submittedOn: "1000-01-01T00:00:00.000Z",
                reviewsReceivedCount: 0
              }
            ],
            reAttemptCount: 0,
            actualSubmit: false,
            userId_contentId:
              "5b8ec10ffd9b940019fa084e_5e32cd795f63ade9c18151e5",
            userId_offerId: "5b8ec10ffd9b940019fa084e_5e268a86bee9d9455beb0932"
          }
        },
        {
          cardQuestion: {
            _id: "5e32c2915f63ade9c18151d2",
            groupIds: [],
            dynamicGroupIds: [],
            customProperties: [],
            versionLogData: {
              _id: "5e32d29895e1202b7569646f"
            },
            orderOfCompletion: "anyOrder",
            deleted: false,
            progressWeight: 1,
            childCompanyIds: [],
            minProgressMeasure: 1,
            contributeToCompleteStatus: true,
            contributeToSuccessStatus: false,
            modelType: "offer",
            publishedVersion: 1,
            version: 0,
            curriculumOfferIds: [],
            coAdminIds: [],
            autoAssignUserGroupIds: [],
            offerType: "cardQuestion",
            children: [],
            completionCriteria: "minProgressMeasure",
            isGradable: true,
            minScore: 0,
            maxScore: 10,
            rootOfferId: "5e268a86bee9d9455beb0932",
            content: {
              modelType: "content",
              childrenIds: [],
              _id: "5e32d29895e1201958696470",
              companyId: "56bfef41a17441a959d9c8da",
              contentType: "cardQuestion",
              gradingScheme: "autoGradable",
              question: {
                responseType: ["fillInTheBlanks"],
                questionText: "Fill the blanks with appropriate options",
                ftbComponents: [
                  {
                    _id: "5e32c53f5f63ade9c18151e1",
                    text:
                      "Luis uses beads to make key chains. Each key chain uses 7 beads. He has 44 beads.",
                    distractors: [],
                    isBlank: false
                  },
                  {
                    _id: "5e32c53f5f63ade9c1815561",
                    text: "How many key chains can Luis make?",
                    distractors: [],
                    isBlank: false
                  },
                  {
                    _id: "5e32c4cc5f63ade9c18151dc",
                    text: "6",
                    distractors: [],
                    isBlank: true
                  },
                  {
                    _id: "5e32c51a5f63ade9c18151df",
                    text: "How many beads will be left over?",
                    distractors: [],
                    isBlank: false
                  },
                  {
                    _id: "5e32c51b5f63ade9c18151e0",
                    text: "2",
                    distractors: [],
                    isBlank: true
                  }
                ],
                globalDistractors: [
                  {
                    text: "7",
                    _id: "5e32c2915f63ade9c18151d7"
                  },
                  {
                    text: "0",
                    _id: "5e32c2915f63ade9c18151d8"
                  },
                  {
                    _id: "5e32c3895f63ade9c18151da",
                    text: "5"
                  },
                  {
                    _id: "5e32c38f5f63ade9c18151db",
                    text: "1"
                  }
                ]
              },
              blockInfo: []
            },
            completionStatusRollupRules: [],
            successStatusRollupRules: [],
            prerequisites: [],
            peerQuestions: []
          },
          state: {
            contentType: "cardQuestion",
            userId: "5b8ec10ffd9b940019fa084e",
            rootContentId_contentId:
              "5e268a86bee9d9455beb0932_5e32c2915f63ade9c18151d2",
            companyId: "56bfef41a17441a959d9c8da",
            contentId: "5e32c2915f63ade9c18151d2",
            rawScore: 0,
            scaledScore: 0,
            progressMeasure: 0,
            scaledProgressMeasure: 0,
            completionStatus: "notAttempted",
            successStatus: "na",
            forceComplete: false,
            timeSpent: 0,
            submissionCount: 0,
            lastUpdatedTimeSpent: "1000-01-01T00:00:00.000Z",
            lastSubmittedOn: "1000-01-01T00:00:00.000Z",
            createdAt: "2020-01-30T12:58:44.580Z",
            updatedAt: "2020-01-30T12:58:44.580Z",
            startedOn: "1000-01-01T00:00:00.000Z",
            lastAccessedOn: "1000-01-01T00:00:00.000Z",
            successOn: "1000-01-01T00:00:00.000Z",
            completedOn: "1000-01-01T00:00:00.000Z",
            versionNumber: 0,
            submissions: [
              {
                _id: "32c4dbc9-5ec7-4fa4-881b-b1c969489511",
                rawScore: 0,
                gradedBy: null,
                progressMeasure: 0,
                response: "",
                answerEvaluation: "",
                responseType: "",
                submittedOn: "1000-01-01T00:00:00.000Z",
                reviewsReceivedCount: 0
              }
            ],
            reAttemptCount: 0,
            actualSubmit: false,
            userId_contentId:
              "5b8ec10ffd9b940019fa084e_5e32c2915f63ade9c18151d2",
            userId_offerId: "5b8ec10ffd9b940019fa084e_5e268a86bee9d9455beb0932"
          }
        },
        {
          cardQuestion: {
            _id: "5e32c6cc5f63ade9c18151e3",
            groupIds: [],
            dynamicGroupIds: [],
            customProperties: [],
            versionLogData: {
              _id: "5e32d29895e1206be469646c"
            },
            orderOfCompletion: "anyOrder",
            deleted: false,
            progressWeight: 1,
            childCompanyIds: [],
            minProgressMeasure: 1,
            contributeToCompleteStatus: true,
            contributeToSuccessStatus: false,
            modelType: "offer",
            publishedVersion: 1,
            version: 0,
            curriculumOfferIds: [],
            coAdminIds: [],
            autoAssignUserGroupIds: [],
            offerType: "cardQuestion",
            children: [],
            completionCriteria: "minProgressMeasure",
            isGradable: true,
            minScore: 0,
            maxScore: 10,
            rootOfferId: "5e268a86bee9d9455beb0932",
            content: {
              modelType: "content",
              childrenIds: [],
              _id: "5e32d29895e120183969646d",
              companyId: "56bfef41a17441a959d9c8da",
              contentType: "cardQuestion",
              gradingScheme: "autoGradable",
              question: {
                responseType: ["multipleSelect"],
                questionText:
                  "<p>Select all the answer choices that show a correct comparison.</p>",
                options: [
                  {
                    optionText:
                      "<span class='fraction'><span class='top'>1</span><span class='bottom'>2</span></span><span>&nbsp;&nbsp;&lt;&nbsp;&nbsp;</span><span class='fraction'><span class='top'>1</span><span class='bottom'>5</span></span>",
                    optionEvaluation: false
                  },
                  {
                    optionText:
                      "<span class='fraction'><span class='top'>3</span><span class='bottom'>5</span></span><span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;</span><span class='fraction'><span class='top'>3</span><span class='bottom'>10</span></span>",
                    optionEvaluation: true
                  },
                  {
                    optionText:
                      "<span class='fraction'><span class='top'>2</span><span class='bottom'>8</span></span><span>&nbsp;&nbsp;&lt;&nbsp;&nbsp;</span><span class='fraction'><span class='top'>1</span><span class='bottom'>4</span></span>",
                    optionEvaluation: false
                  },
                  {
                    optionText:
                      "<span class='fraction'><span class='top'>5</span><span class='bottom'>6</span></span><span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;</span><span class='fraction'><span class='top'>2</span><span class='bottom'>5</span></span>",
                    optionEvaluation: true
                  },
                  {
                    optionText:
                      "<span class='fraction'><span class='top'>5</span><span class='bottom'>10</span></span><span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;</span><span class='fraction'><span class='top'>2</span><span class='bottom'>5</span></span>",
                    optionEvaluation: true
                  }
                ]
              },
              blockInfo: []
            },
            completionStatusRollupRules: [],
            successStatusRollupRules: [],
            prerequisites: [],
            peerQuestions: []
          },
          state: {
            contentType: "cardQuestion",
            userId: "5b8ec10ffd9b940019fa084e",
            rootContentId_contentId:
              "5e268a86bee9d9455beb0932_5e32c6cc5f63ade9c18151e3",
            companyId: "56bfef41a17441a959d9c8da",
            contentId: "5e32c6cc5f63ade9c18151e3",
            rawScore: 0,
            scaledScore: 0,
            progressMeasure: 0,
            scaledProgressMeasure: 0,
            completionStatus: "notAttempted",
            successStatus: "na",
            forceComplete: false,
            timeSpent: 0,
            submissionCount: 0,
            lastUpdatedTimeSpent: "1000-01-01T00:00:00.000Z",
            lastSubmittedOn: "1000-01-01T00:00:00.000Z",
            createdAt: "2020-01-30T12:58:44.580Z",
            updatedAt: "2020-01-30T12:58:44.580Z",
            startedOn: "1000-01-01T00:00:00.000Z",
            lastAccessedOn: "1000-01-01T00:00:00.000Z",
            successOn: "1000-01-01T00:00:00.000Z",
            completedOn: "1000-01-01T00:00:00.000Z",
            versionNumber: 0,
            submissions: [
              {
                _id: "b6dbfab5-5134-4d7c-8348-7911bd85d57f",
                rawScore: 0,
                gradedBy: null,
                progressMeasure: 0,
                response: "",
                answerEvaluation: "",
                responseType: "",
                submittedOn: "1000-01-01T00:00:00.000Z",
                reviewsReceivedCount: 0
              }
            ],
            reAttemptCount: 0,
            actualSubmit: false,
            userId_contentId:
              "5b8ec10ffd9b940019fa084e_5e32c6cc5f63ade9c18151e3",
            userId_offerId: "5b8ec10ffd9b940019fa084e_5e268a86bee9d9455beb0932"
          }
        },
        {
          cardQuestion: {
            _id: "5e32cf745f63ade9c18151e6",
            groupIds: [],
            dynamicGroupIds: [],
            customProperties: [],
            versionLogData: {
              _id: "5e32d29895e1206fb2696469"
            },
            orderOfCompletion: "anyOrder",
            deleted: false,
            progressWeight: 1,
            childCompanyIds: [],
            minProgressMeasure: 1,
            contributeToCompleteStatus: true,
            contributeToSuccessStatus: false,
            modelType: "offer",
            publishedVersion: 1,
            version: 0,
            curriculumOfferIds: [],
            coAdminIds: [],
            autoAssignUserGroupIds: [],
            offerType: "cardQuestion",
            children: [],
            completionCriteria: "minProgressMeasure",
            isGradable: true,
            minScore: 0,
            maxScore: 10,
            rootOfferId: "5e268a86bee9d9455beb0932",
            content: {
              modelType: "content",
              childrenIds: [],
              _id: "5e32d29895e1206fd569646a",
              companyId: "56bfef41a17441a959d9c8da",
              contentType: "cardQuestion",
              gradingScheme: "autoGradable",
              question: {
                responseType: ["fillInTheBlanks"],
                questionText: "Fill the blanks with appropriate options",
                ftbComponents: [
                  {
                    text:
                      "Classify triangle ABD by its sides and then by its angles.",
                    distractors: [],
                    isBlank: false,
                    _id: "5e32cf745f63ade9c18151e7"
                  },
                  {
                    text: "Triangle ABD is",
                    distractors: [],
                    isBlank: false,
                    _id: "5e32cf745f63ade9c18151e7"
                  },
                  {
                    text: "Scalene Triangle",
                    distractors: [],
                    isBlank: true,
                    _id: "5e32cf745f63ade9c18151e8"
                  },
                  {
                    text: "and",
                    distractors: [],
                    isBlank: false,
                    _id: "5e32cf745f63ade9c18151e9"
                  },
                  {
                    text: "Right triangle",
                    distractors: [],
                    isBlank: true,
                    _id: "5e32cf745f63ade9c18151ea"
                  },
                  {
                    text:
                      "<br><img src='https://dndw32r4edmw5.cloudfront.net/assets/frontend-js/fonts/assessment-player/5e32cf745f63ade9c18151e6.png' alt='Figure' />",
                    distractors: [],
                    isBlank: false,
                    _id: "5e32cf745f63ade9c18151e9"
                  }
                ],
                globalDistractors: [
                  {
                    text: "Obtuse Triangle",
                    _id: "5e32cf745f63ade9c18151eb"
                  },
                  {
                    text: "Equilateral triangle",
                    _id: "5e32cf745f63ade9c18151ec"
                  },
                  {
                    _id: "5e32cfae5f63ade9c18151ed",
                    text: "Acute Triangle"
                  },
                  {
                    _id: "5e32cfaf5f63ade9c18151ee",
                    text: "Isosceles triangle"
                  }
                ]
              },
              blockInfo: []
            },
            completionStatusRollupRules: [],
            successStatusRollupRules: [],
            prerequisites: [],
            peerQuestions: []
          },
          state: {
            contentType: "cardQuestion",
            userId: "5b8ec10ffd9b940019fa084e",
            rootContentId_contentId:
              "5e268a86bee9d9455beb0932_5e32cf745f63ade9c18151e6",
            companyId: "56bfef41a17441a959d9c8da",
            contentId: "5e32cf745f63ade9c18151e6",
            rawScore: 0,
            scaledScore: 0,
            progressMeasure: 0,
            scaledProgressMeasure: 0,
            completionStatus: "notAttempted",
            successStatus: "na",
            forceComplete: false,
            timeSpent: 0,
            submissionCount: 0,
            lastUpdatedTimeSpent: "1000-01-01T00:00:00.000Z",
            lastSubmittedOn: "1000-01-01T00:00:00.000Z",
            createdAt: "2020-01-30T12:58:44.580Z",
            updatedAt: "2020-01-30T12:58:44.580Z",
            startedOn: "1000-01-01T00:00:00.000Z",
            lastAccessedOn: "1000-01-01T00:00:00.000Z",
            successOn: "1000-01-01T00:00:00.000Z",
            completedOn: "1000-01-01T00:00:00.000Z",
            versionNumber: 0,
            submissions: [
              {
                _id: "95d17624-f3d2-4057-a272-d1c73a0f697e",
                rawScore: 0,
                gradedBy: null,
                progressMeasure: 0,
                response: "",
                answerEvaluation: "",
                responseType: "",
                submittedOn: "1000-01-01T00:00:00.000Z",
                reviewsReceivedCount: 0
              }
            ],
            reAttemptCount: 0,
            actualSubmit: false,
            userId_contentId:
              "5b8ec10ffd9b940019fa084e_5e32cf745f63ade9c18151e6",
            userId_offerId: "5b8ec10ffd9b940019fa084e_5e268a86bee9d9455beb0932"
          }
        },
        {
          cardQuestion: {
            _id: "5e32d1205f63ade9c18151ef",
            groupIds: [],
            dynamicGroupIds: [],
            customProperties: [],
            versionLogData: {
              _id: "5e32d29895e1202f1c696466"
            },
            orderOfCompletion: "anyOrder",
            deleted: false,
            progressWeight: 1,
            childCompanyIds: [],
            minProgressMeasure: 1,
            contributeToCompleteStatus: true,
            contributeToSuccessStatus: false,
            modelType: "offer",
            publishedVersion: 1,
            version: 0,
            curriculumOfferIds: [],
            coAdminIds: [],
            autoAssignUserGroupIds: [],
            offerType: "cardQuestion",
            children: [],
            completionCriteria: "minProgressMeasure",
            isGradable: true,
            minScore: 0,
            maxScore: 10,
            rootOfferId: "5e268a86bee9d9455beb0932",
            content: {
              modelType: "content",
              childrenIds: [],
              _id: "5e32d29895e1203abf696467",
              companyId: "56bfef41a17441a959d9c8da",
              contentType: "cardQuestion",
              gradingScheme: "autoGradable",
              question: {
                responseType: ["singleSelect"],
                questionText:
                  "<p>Zoe uses 1 <sup>1</sup>&frasl;<sub>4</sub> cups of flour in each batch of cookies. How much flour will she use in 6 batches?</p>",
                options: [
                  {
                    optionText: "<p>7 <sup>1</sup>&frasl;<sub>4</sub> cups</p>",
                    optionEvaluation: false
                  },
                  {
                    optionText: "<p>7 <sup>1</sup>&frasl;<sub>2</sub> cups</p>",
                    optionEvaluation: true
                  },
                  {
                    optionText: "<p>8 <sup>1</sup>&frasl;<sub>2</sub> cups</p>",
                    optionEvaluation: false
                  },
                  {
                    optionText: "<p>9 cups</p>",
                    optionEvaluation: false
                  }
                ]
              },
              blockInfo: []
            },
            completionStatusRollupRules: [],
            successStatusRollupRules: [],
            prerequisites: [],
            peerQuestions: []
          },
          state: {
            contentType: "cardQuestion",
            userId: "5b8ec10ffd9b940019fa084e",
            rootContentId_contentId:
              "5e268a86bee9d9455beb0932_5e32d1205f63ade9c18151ef",
            companyId: "56bfef41a17441a959d9c8da",
            contentId: "5e32d1205f63ade9c18151ef",
            rawScore: 0,
            scaledScore: 0,
            progressMeasure: 0,
            scaledProgressMeasure: 0,
            completionStatus: "notAttempted",
            successStatus: "na",
            forceComplete: false,
            timeSpent: 0,
            submissionCount: 0,
            lastUpdatedTimeSpent: "1000-01-01T00:00:00.000Z",
            lastSubmittedOn: "1000-01-01T00:00:00.000Z",
            createdAt: "2020-01-30T12:58:44.580Z",
            updatedAt: "2020-01-30T12:58:44.580Z",
            startedOn: "1000-01-01T00:00:00.000Z",
            lastAccessedOn: "1000-01-01T00:00:00.000Z",
            successOn: "1000-01-01T00:00:00.000Z",
            completedOn: "1000-01-01T00:00:00.000Z",
            versionNumber: 0,
            submissions: [
              {
                _id: "30bc816d-6724-41a4-9b98-e408c847b961",
                rawScore: 0,
                gradedBy: null,
                progressMeasure: 0,
                response: "",
                answerEvaluation: "",
                responseType: "",
                submittedOn: "1000-01-01T00:00:00.000Z",
                reviewsReceivedCount: 0
              }
            ],
            reAttemptCount: 0,
            actualSubmit: false,
            userId_contentId:
              "5b8ec10ffd9b940019fa084e_5e32d1205f63ade9c18151ef",
            userId_offerId: "5b8ec10ffd9b940019fa084e_5e268a86bee9d9455beb0932"
          }
        },
        {
          cardQuestion: {
            _id: "5e32d1e35f63ade9c18151f0",
            groupIds: [],
            dynamicGroupIds: [],
            customProperties: [],
            versionLogData: {
              _id: "5e32d29895e12078ef696463"
            },
            orderOfCompletion: "anyOrder",
            deleted: false,
            progressWeight: 1,
            childCompanyIds: [],
            minProgressMeasure: 1,
            contributeToCompleteStatus: true,
            contributeToSuccessStatus: false,
            modelType: "offer",
            publishedVersion: 1,
            version: 0,
            curriculumOfferIds: [],
            coAdminIds: [],
            autoAssignUserGroupIds: [],
            offerType: "cardQuestion",
            children: [],
            completionCriteria: "minProgressMeasure",
            isGradable: true,
            minScore: 0,
            maxScore: 10,
            rootOfferId: "5e268a86bee9d9455beb0932",
            content: {
              modelType: "content",
              childrenIds: [],
              _id: "5e32d29895e120410c696464",
              companyId: "56bfef41a17441a959d9c8da",
              contentType: "cardQuestion",
              gradingScheme: "autoGradable",
              question: {
                responseType: ["fillInTheBlanks"],
                questionText: "Fill the blanks with appropriate options",
                ftbComponents: [
                  {
                    text:
                      "Use the Distributive Property to find the product of 6 * 3,584.",
                    distractors: [],
                    isBlank: false,
                    _id: "5e32d1e35f63ade9c18151f1"
                  },
                  {
                    text: "Select your answer.",
                    distractors: [],
                    isBlank: false,
                    _id: "5e32d1e35f63ade9c18151f1"
                  },
                  {
                    text: "21,504",
                    distractors: [],
                    isBlank: true,
                    _id: "5e32d1e35f63ade9c18151f2"
                  }
                ],
                globalDistractors: [
                  {
                    text: "21,304",
                    _id: "5e32d1e35f63ade9c18151f5"
                  },
                  {
                    text: "22,504",
                    _id: "5e32d1e35f63ade9c18151f6"
                  },
                  {
                    _id: "5e32d2425f63ade9c18151f7",
                    text: "20,504"
                  }
                ]
              },
              blockInfo: []
            },
            completionStatusRollupRules: [],
            successStatusRollupRules: [],
            prerequisites: [],
            peerQuestions: []
          },
          state: {
            contentType: "cardQuestion",
            userId: "5b8ec10ffd9b940019fa084e",
            rootContentId_contentId:
              "5e268a86bee9d9455beb0932_5e32d1e35f63ade9c18151f0",
            companyId: "56bfef41a17441a959d9c8da",
            contentId: "5e32d1e35f63ade9c18151f0",
            rawScore: 0,
            scaledScore: 0,
            progressMeasure: 0,
            scaledProgressMeasure: 0,
            completionStatus: "notAttempted",
            successStatus: "na",
            forceComplete: false,
            timeSpent: 0,
            submissionCount: 0,
            lastUpdatedTimeSpent: "1000-01-01T00:00:00.000Z",
            lastSubmittedOn: "1000-01-01T00:00:00.000Z",
            createdAt: "2020-01-30T12:58:44.580Z",
            updatedAt: "2020-01-30T12:58:44.580Z",
            startedOn: "1000-01-01T00:00:00.000Z",
            lastAccessedOn: "1000-01-01T00:00:00.000Z",
            successOn: "1000-01-01T00:00:00.000Z",
            completedOn: "1000-01-01T00:00:00.000Z",
            versionNumber: 0,
            submissions: [
              {
                _id: "2c1942f3-6d04-447a-87c5-b2739d0bb25f",
                rawScore: 0,
                gradedBy: null,
                progressMeasure: 0,
                response: "",
                answerEvaluation: "",
                responseType: "",
                submittedOn: "1000-01-01T00:00:00.000Z",
                reviewsReceivedCount: 0
              }
            ],
            reAttemptCount: 0,
            actualSubmit: false,
            userId_contentId:
              "5b8ec10ffd9b940019fa084e_5e32d1e35f63ade9c18151f0",
            userId_offerId: "5b8ec10ffd9b940019fa084e_5e268a86bee9d9455beb0932"
          }
        }
      ],
      successStatusRollupRules: [],
      randomizeQuestions: true,
      successCriteria: "minScaledScore",
      minScaledScore: 0.8,
      maxScore: 100,
      minScore: 0,
      sixtySecWarning: false,
      timeBoundTest: true,
      prerequisites: []
    },
    state: {
      contentType: "assignmentCard",
      userId: "5b8ec10ffd9b940019fa084e",
      rootContentId_contentId:
        "5e268a86bee9d9455beb0932_5e32bf945f63ade9c18151ce",
      companyId: "56bfef41a17441a959d9c8da",
      contentId: "5e32bf945f63ade9c18151ce",
      rawScore: 0,
      scaledScore: 0,
      progressMeasure: 0,
      scaledProgressMeasure: 0,
      completionStatus: "notAttempted",
      successStatus: "na",
      forceComplete: false,
      timeSpent: 0,
      submissionCount: 0,
      lastUpdatedTimeSpent: "1000-01-01T00:00:00.000Z",
      lastSubmittedOn: "1000-01-01T00:00:00.000Z",
      createdAt: "2020-01-30T12:58:44.580Z",
      updatedAt: "2020-01-30T12:58:44.580Z",
      startedOn: "1000-01-01T00:00:00.000Z",
      lastAccessedOn: "1000-01-01T00:00:00.000Z",
      successOn: "1000-01-01T00:00:00.000Z",
      completedOn: "1000-01-01T00:00:00.000Z",
      versionNumber: 0,
      reAttemptCount: 0,
      actualSubmit: false,
      userId_contentId: "5b8ec10ffd9b940019fa084e_5e32bf945f63ade9c18151ce",
      userId_offerId: "5b8ec10ffd9b940019fa084e_5e268a86bee9d9455beb0932"
    }
  }
};

export default props;
