const AnswerEvaluationReverse = {
  NA: "na",
  TRUE: "true",
  FALSE: "false"
};

const ResponseTypeReverse = {
  VIDEO: "video",
  AUDIO: "audio",
  TEXT: "text",
  IMAGE: "image",
  SINGLE_SELECT: "singleSelect",
  MULTIPLE_SELECT: "multipleSelect",
  MATCH_THE_FOLLOWING: "matchTheFollowing",
  FILL_IN_THE_BLANKS: "fillInTheBlanks",
  EMPTY: "",
  OPINION_SCALE: "opinionScale",
  FILE: "file"
};

export const mtfFunction = {
  getCorrectAnsObj: pairs => {
    const correctAnsObj = {};
    pairs.forEach(item => {
      correctAnsObj[item.left._id] = item.right._id;
    });
    return correctAnsObj;
  },
  getSubmittedResponseObj: submittedResponseArr => {
    const submittedResponseObj = {};
    submittedResponseArr.forEach(item => {
      submittedResponseObj[item.leftId] = item.rightId || "";
    });
    return submittedResponseObj;
  },
  evalMTFAns: (submittedResponseArr, pairs) => {
    let correctAns = true;
    const submittedAnsObj = mtfFunction.getSubmittedResponseObj(
      submittedResponseArr
    );
    const correctAnsObj = mtfFunction.getCorrectAnsObj(pairs);
    Object.keys(submittedAnsObj).forEach(leftItem => {
      if (submittedAnsObj[leftItem] !== correctAnsObj[leftItem]) {
        correctAns = false;
      }
    });
    return correctAns.toString().toUpperCase();
  }
};

export const ftbFunction = {
  evalFTBAnswer: mappedClickedObj => {
    let evalAns = true;
    Object.keys(mappedClickedObj).forEach(objKey => {
      if (objKey !== mappedClickedObj[objKey]) {
        evalAns = false;
      }
    });
    return evalAns.toString().toUpperCase();
  }
};

export const evaluateSelectResponse = (responseString, options) => {
  const parsedResponse = responseString.split(",");
  const isCorrect = options.every(({ optionEvaluation }, index) => {
    if (optionEvaluation) {
      return parsedResponse.includes(index.toString());
    }
    return !parsedResponse.includes(index.toString());
  });
  return isCorrect.toString().toUpperCase();
};

export const formatAnswerForCommand = answer => ({
  ...answer,
  answerEvaluation: AnswerEvaluationReverse[answer.answerEvaluation],
  responseType: ResponseTypeReverse[answer.responseType]
});

export function getScaledScoreForRawScore(rawScore, minScore, maxScore) {
  return maxScore - minScore > 0
    ? (rawScore - minScore) / (maxScore - minScore)
    : 0;
}

export function getSuccessStatusForLeafNode(
  successCriteria,
  scaledScore,
  minScaledScore,
  completionStatus,
  isGraded
) {
  switch (successCriteria) {
    case "minScaledScore": {
      if (isGraded && completionStatus === "complete") {
        if (scaledScore >= minScaledScore) {
          return "pass";
        }
        return "fail";
      }
      return "na";
    }
    default:
      console.info("Invalid success criteria" + successCriteria);
  }
  return "na";
}
